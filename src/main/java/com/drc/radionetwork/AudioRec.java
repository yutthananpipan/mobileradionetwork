package com.drc.radionetwork;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Process;
import android.util.Log;
import android.widget.Toast;

import com.drc.radionetwork.utils.Constanst;
import com.drc.radionetwork.utils.Util;
import com.drc.radionetwork.onnetwork.network.NetworkProp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.ArrayList;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class AudioRec implements Runnable {
    private static final String LOG_TAG = AudioRec.class.getSimpleName();
    private final AudioRecord m_audioRec;

    private final Thread m_thread;
    private final ReentrantLock m_lock;
    private final Condition m_cond;

    private int m_state;
    private boolean m_ptt;
    private boolean isMute = false;

    private static final int IDLE = 0;
    private static final int START = 1;
    private static final int RUN = 2;
    private static final int STOP = 3;
    private static final int SHTDN = 4; // shutdown

    public static final int SAMPLE_RATE = 16000;
    public static final int FRAME_SIZE = 1024;

    // 1 or 2
    static final int NUM_CHANNELS = 1;

    public boolean isPtt(){
        return m_ptt;
    }

    private DatagramSocket socket;
    private ArrayList<NetworkProp.NetworkAdpaterInfo> addresses;
    private final Context context;

    public boolean isMute() {
        return isMute;
    }

    public void setMute(boolean mute) {
        isMute = mute;
    }

    public enum Mode {
        UNICAST,
        BROADCAST
    }

    private String destination = "";

    public ArrayList<NetworkProp.NetworkAdpaterInfo> getAddresses() {
        return addresses;
    }

    public AudioRec(AudioRecord m_audioRec, Context context) {
        this.m_audioRec = m_audioRec;
        this.context = context;

        BroadcastReceiver wifireciever = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                addresses = NetworkProp.getInstance().getAdaptersAddress();
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        filter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        filter.addAction("android.net.wifi.STATE_CHANGE");

        context.registerReceiver(wifireciever, filter);

        try {
            socket = new MulticastSocket();
            addresses = NetworkProp.getInstance().getAdaptersAddress();
        } catch (IOException e) {
            e.printStackTrace();
        }

        m_thread = new Thread(this, LOG_TAG + " [" + "OPUS:44100" + "]");

        m_lock = new ReentrantLock();
        m_cond = m_lock.newCondition();
        m_state = IDLE;
        m_thread.start();
    }

    @Override
    public void run() {
        android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_URGENT_AUDIO);
        boolean interrupted = false;

        try {
            for (; ; ) {
                m_lock.lock();
                try {
                    while (m_state == IDLE)
                        m_cond.await();

                    if (m_state == START) {
                        m_audioRec.startRecording();
                        m_state = RUN;
                    } else if (m_state == STOP) {
                        m_audioRec.stop();
                        m_state = IDLE;

                        //Log.i( LOG_TAG, "Sent " + frames + " frames." );
                        continue;
                    } else if (m_state == SHTDN)
                        break;

                } finally {
                    m_lock.unlock();
                }

                try {
                    if (destination.isEmpty() || isMute()) {
                        continue;
                    }

                    byte[] inBuf = new byte[FRAME_SIZE * NUM_CHANNELS];

                    // Encoder must be fed entire frames.
                    int len = m_audioRec.read(inBuf, 0, inBuf.length);

                    Log.v(LOG_TAG, "Encoded " + inBuf.length + " bytes of audio into " + len + " bytes");

                    try {
                        InetAddress inetAddress = InetAddress.getByName(destination);
                        DatagramPacket packet = new DatagramPacket(inBuf, inBuf.length, inetAddress, Constanst.AUDIO_PORT);
                        socket.send(packet);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

        } catch (final InterruptedException ex) {
            Log.e(LOG_TAG, ex.toString(), ex);
            interrupted = true;
        }

        m_audioRec.stop();
        m_audioRec.release();

        if (interrupted)
            Thread.currentThread().interrupt();

    }

    public void setPTT(boolean ptt) {
        m_lock.lock();
        try {
            m_ptt = ptt;
        } finally {
            m_lock.unlock();
        }
    }

    public void startRecording(boolean ptt) {
        //Open led red
        Util.LED_Red_Control(context, true);
        destination = Util.getDestination(context);

        AudioPlayer audioPlayer = WalkieService.audioPlayer;
        if (audioPlayer != null) {
            if (audioPlayer.isDataComing) {
                Toast.makeText(this.context, "ผู้ใช้ท่านอื่นกำลังใช้งาน กรุณารอสักครู่", Toast.LENGTH_LONG).show();
            }else {
                Log.d(LOG_TAG, "startRecording");
                m_lock.lock();
                try {
                    if (m_state == IDLE) {
                        m_state = START;
                        m_cond.signal();
                    } else if (m_state == STOP)
                        m_state = RUN;

                    this.m_ptt = ptt;
                } finally {
                    m_lock.unlock();
                }
            }
        }
    }

    public void stopRecording() {
        Log.d(LOG_TAG, "stopRecording");
        //Close led red
        Util.LED_Red_Control(context, false);

        m_lock.lock();
        try {
            if (m_state != IDLE)
                m_state = STOP;
        } finally {
            m_lock.unlock();
        }
    }

    public void shutdown() {
        Log.d(LOG_TAG, "shutdown");
        m_lock.lock();
        try {
            if (m_state == IDLE)
                m_cond.signal();
            m_state = SHTDN;
        } finally {
            m_lock.unlock();
        }

        try {
            m_thread.join();
        } catch (final InterruptedException ex) {
            Log.e(LOG_TAG, ex.toString(), ex);
        }

    }

    public static AudioRec create(Context context) {
        try {
            int minBufSize = AudioRecord.getMinBufferSize(SAMPLE_RATE,
                    AudioFormat.CHANNEL_IN_MONO,
                    AudioFormat.ENCODING_PCM_16BIT);

            // initialize audio recorder
            AudioRecord recorder = new AudioRecord(
                    MediaRecorder.AudioSource.MIC,
                    SAMPLE_RATE,
                    AudioFormat.CHANNEL_IN_MONO,
                    AudioFormat.ENCODING_PCM_16BIT,
                    minBufSize);

            return new AudioRec(recorder, context);
        } catch (final NumberFormatException ex) {
            Log.e(LOG_TAG, ex.toString());
        }
        return null;
    }
}
