package com.drc.radionetwork;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.drc.radionetwork.model.ListItems;
import com.drc.radionetwork.model.Radio;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.List;

public class ClientManageAdapter extends ArrayAdapter<ListItems> {

    private final List<ListItems> data;

    private static class ViewHolder {
        public MaterialSpinner Chanel = null;
        public EditText Name = null;
        public ImageView btnEdit = null;
        public RelativeLayout content_header = null;
        public RelativeLayout content_client = null;

        public static ClientManageAdapter.ViewHolder createViewHolder(View view) {
            ViewHolder holder = new ViewHolder();
            holder.Chanel = view.findViewById(R.id.ch_spinner);
            holder.Name = view.findViewById(R.id.name_edit);
            holder.btnEdit = view.findViewById(R.id.btnEdit);
            holder.content_header = view.findViewById(R.id.content_header);
            holder.content_client = view.findViewById(R.id.content_client);
            return holder;
        }
    }

    public ClientManageAdapter(@NonNull Context context, @NonNull List<ListItems> data) {
        super(context, 0, data);
        this.data = data;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ListItems item = data.get(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listview_client_setting, parent, false);
            convertView.setTag(ViewHolder.createViewHolder(convertView));
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();

        if (item.isHeader) {
            holder.content_header.setVisibility(View.VISIBLE);
            holder.content_client.setVisibility(View.GONE);
        } else {
            holder.content_header.setVisibility(View.GONE);
            holder.content_client.setVisibility(View.VISIBLE);

            List<String> chanels = Radio.getChanel();
            holder.Chanel.setItems(chanels);
            holder.Chanel.setSelectedIndex(0);
            holder.Chanel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.Chanel.setBackgroundColor(getContext().getResources().getColor(R.color.background_color));
                }
            });
            holder.Chanel.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                @Override
                public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                    holder.Chanel.setBackgroundColor(getContext().getResources().getColor(R.color.transparent));
                }
            });
            holder.Chanel.setOnNothingSelectedListener(new MaterialSpinner.OnNothingSelectedListener() {
                @Override
                public void onNothingSelected(MaterialSpinner spinner) {
                    holder.Chanel.setBackgroundColor(getContext().getResources().getColor(R.color.transparent));
                }
            });

            holder.Name.setText(item.getName());

            holder.btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return super.getDropDownView(position, convertView, parent);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Nullable
    @Override
    public ListItems getItem(int position) {
        return super.getItem(position);
    }
}
