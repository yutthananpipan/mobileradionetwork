package com.drc.radionetwork;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.util.Log;

import com.drc.radionetwork.onnetwork.OnNetwork;
import com.drc.radionetwork.onnetwork.network.NetworkProp;
import com.drc.radionetwork.utils.JSONDataUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class TrackingService extends Service implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private static final String LOG_TAG = TrackingService.class.getSimpleName().toString();
    private static TrackingService instance;

    private FusedLocationProviderClient fusedLocationClient;
    private GoogleApiClient googleApiClient;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private LocationRequest locationRequest;
    private static final long UPDATE_INTERVAL = 5000, FASTEST_INTERVAL = 5000; // = 5 seconds

    private LocationCallback locationCallback;
    private int BatterLevel = 0;

    // flag for GPS status
    boolean isGPSEnabled = false;

    // flag for network status
    boolean isNetworkEnabled = false;

    // flag for GPS status
    boolean canGetLocation = false;

    Location location; // location
    double latitude; // latitude
    double longitude; // longitude

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute

    // Declaring a Location Manager
    protected LocationManager locationManager;

    private Handler handler;

    public TrackingService() {
    }

    public static TrackingService getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.registerReceiver(this.mBatInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

    }

    private BroadcastReceiver mBatInfoReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            BatterLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
        }
    };

    public Location getLocation() {
        try {
            locationManager = (LocationManager) getApplicationContext()
                    .getSystemService(LOCATION_SERVICE);

            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
            } else {
                this.canGetLocation = true;
                // First get location from Network Provider
                if (isNetworkEnabled) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return null;
                    }
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("Network", "Network");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        Log.d("GPS Enabled", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (CheckPermission()) {
            if (getInstance() == null) {
                instance = this;

                handler = new Handler();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Location locate = getLocation();
                        if (locate != null) {
                            try {
                                ArrayList<NetworkProp.NetworkAdpaterInfo> addresses = WalkieService.getInstance().getAudioRecorder().getAddresses();
                                NetworkProp.NetworkAdpaterInfo info = addresses.get(0);

                                @SuppressLint("HardwareIds") String androidId = Settings.Secure.getString(getContentResolver(),
                                        Settings.Secure.ANDROID_ID);

                                JSONObject obj = new JSONObject();

                                obj.put("name", info.getIpAddress().split("\\.")[info.getIpAddress().split("\\.").length - 1]);
                                obj.put("lat", locate.getLatitude());
                                obj.put("lng", locate.getLongitude());
                                obj.put("battery", BatterLevel);
                                obj.put("deviceId", androidId);
                                obj.put("ip", info.getIpAddress());

                                OnNetwork onNetwork = OnNetwork.generate();
                                onNetwork.sendBroadCast(NetworkProp.getInstance().getAdaptersAddress(), "android", JSONDataUtils.Action.Set.toString(), obj.toString());

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                e.toString();
                            }
                        } else {
                            try {
                                ArrayList<NetworkProp.NetworkAdpaterInfo> addresses = WalkieService.getInstance().getAudioRecorder().getAddresses();
                                NetworkProp.NetworkAdpaterInfo info = addresses.get(0);

                                @SuppressLint("HardwareIds") String androidId = Settings.Secure.getString(getContentResolver(),
                                        Settings.Secure.ANDROID_ID);

                                JSONObject obj = new JSONObject();

                                obj.put("name", info.getIpAddress().split("\\.")[info.getIpAddress().split("\\.").length - 1]);
                                obj.put("lat", "0.000");
                                obj.put("lng", "0.000");
                                obj.put("battery", BatterLevel);
                                obj.put("deviceId", androidId);
                                obj.put("ip", info.getIpAddress());

                                OnNetwork onNetwork = OnNetwork.generate();
                                onNetwork.sendBroadCast(NetworkProp.getInstance().getAdaptersAddress(), "android", JSONDataUtils.Action.Set.toString(), obj.toString());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (handler != null) {
                            handler.postDelayed(this, 30000);
                        }
                    }
                });
            }
        }

        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        Log.d(LOG_TAG, "onDestroy");

        Log.d(LOG_TAG, "onDestroy: done");

        handler = null;
        this.unregisterReceiver(mBatInfoReceiver);

        instance = null;

        super.onDestroy();
    }

    public boolean CheckPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            return true;
        } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            return true;
        }


        return false;
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(getApplicationContext());

        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                //apiAvailability.getErrorDialog(, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST);

            } else {
                //finish();
            }

            return false;
        }

        return true;
    }

    private void startLocationUpdates() {
        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);

        //LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        // Permissions ok, we get last location
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

        if (location != null) {
            Log.d(LOG_TAG, "Latitude : " + location.getLatitude() + "\nLongitude : " + location.getLongitude());
        }

        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
