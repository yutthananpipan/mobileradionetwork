package com.drc.radionetwork.onnetwork;


import com.drc.radionetwork.model.BroadCastData;

public interface OnNetworkImpl {

    void onBroadCastReceived(BroadCastData data);

}
