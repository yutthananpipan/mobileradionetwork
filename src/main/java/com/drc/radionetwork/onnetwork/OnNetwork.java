package com.drc.radionetwork.onnetwork;

import com.drc.radionetwork.model.BroadCastData;
import com.drc.radionetwork.onnetwork.broadcast.BroadCastDataType;
import com.drc.radionetwork.onnetwork.network.NetworkProp;
import com.google.gson.Gson;

import java.util.ArrayList;

public class OnNetwork {

    private NetworkEngine networkEngine;

    private OnNetwork() {
        networkEngine = NetworkEngine.getInstance();
    }

    public static OnNetwork generate() {
        return new OnNetwork();
    }

    public void sendBroadCast(ArrayList<NetworkProp.NetworkAdpaterInfo> addresses, String name, String action, String value) {
        if (name != null && action != null && addresses != null) {
            Gson gson = new Gson();

            for(NetworkProp.NetworkAdpaterInfo info : addresses) {
                BroadCastData broadCastData = new BroadCastData(name, action, value, BroadCastDataType.DATA_TYPE_FROM_CLIENT);

                String payload = gson.toJson(broadCastData);
                networkEngine.sendBroadCast(info, payload);
            }
        }
    }

    public void receiveBroadCastStart(OnNetworkImpl callback) {
        networkEngine.listenBroadCastStart(callback);
    }

    public void receiveBroadCastStop() {
        networkEngine.listenBroadCastStop();
    }

}
