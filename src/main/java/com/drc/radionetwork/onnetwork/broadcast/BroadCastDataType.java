package com.drc.radionetwork.onnetwork.broadcast;

import com.drc.radionetwork.onnetwork.OnNetworkConstants;

public enum BroadCastDataType {

    DATA_TYPE_FROM_SERVER(OnNetworkConstants.DATA_TYPE_FROM_SERVER),
    DATA_TYPE_FROM_CLIENT(OnNetworkConstants.DATA_TYPE_FROM_CLIENT);

    private String data;

    BroadCastDataType(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return data;
    }
}
