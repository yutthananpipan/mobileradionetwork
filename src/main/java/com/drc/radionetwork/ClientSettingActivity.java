package com.drc.radionetwork;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import com.drc.radionetwork.model.ListItems;
import com.drc.radionetwork.model.Radio;
import com.drc.radionetwork.utils.Util;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ClientSettingActivity extends AppCompatActivity {
    private MaterialSpinner zone_spinner;
    private ClientManageAdapter adapter;
    ArrayList<ListItems> dataSource = new ArrayList<ListItems>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_setting);

        zone_spinner = findViewById(R.id.zone_spinner_setup);
        ListView listView = findViewById(R.id.listView_client_setting);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowHomeEnabled(true);

        adapter = new ClientManageAdapter(this, getItem());
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }

    @Override
    protected void onResume() {
        super.onResume();

        initData();
    }

    private ArrayList<ListItems> getItem() {
        dataSource.add(new ListItems("Channel", "", true));

        dataSource.add(new ListItems("CH1", "UID1", false));
        dataSource.add(new ListItems("CH2", "UID2", false));
        dataSource.add(new ListItems("CH3", "UID3", false));
        dataSource.add(new ListItems("CH4", "UID4", false));
        dataSource.add(new ListItems("CH5", "UID5", false));
        dataSource.add(new ListItems("CH6", "UID6", false));
        dataSource.add(new ListItems("CH7", "UID7", false));
        dataSource.add(new ListItems("CH8", "UID8", false));
        dataSource.add(new ListItems("CH9", "UID1", false));
        dataSource.add(new ListItems("CH10", "UID1", false));

        return dataSource;
    }

    private void initData() {
        try {

            List<String> zones = Radio.getZone();
            zone_spinner.setItems(zones);
            String zone = Util.getZone(getApplicationContext());

            if (zone.isEmpty()) {
                zone_spinner.setSelectedIndex(0);
            } else {
                zone_spinner.setSelectedIndex((zones.contains(zone)) ? zones.indexOf(zone) : 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}