package com.drc.radionetwork;

import android.content.Context;
import android.util.Log;

import com.drc.radionetwork.model.BroadCastData;
import com.drc.radionetwork.model.History;
import com.drc.radionetwork.model.Radio;
import com.drc.radionetwork.onnetwork.broadcast.BroadCastDataType;
import com.drc.radionetwork.utils.Constanst;
import com.drc.radionetwork.utils.JSONDataUtils;
import com.drc.radionetwork.utils.Util;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.List;

public class TCPClient {
    private final Context context;
    private final WalkieService service;
    public Socket socket;

    /**
     * Specify the Server Ip Address here. Whereas our Socket Server is started.
     */
    String TAG = TCPClient.class.getSimpleName();

    private boolean mRun = false;

    private PrintWriter out = null;
    private boolean SocketStatus = false;

    public void setSocketStatus(boolean socketStatus) {
        SocketStatus = socketStatus;
    }

    public boolean isSocketStatus() {
        return SocketStatus;
    }

    public TCPClient(Context context, WalkieService service) {
        this.context = context;
        this.service = service;

    }

    public void sendMessage(String message) {
        if (out != null && !out.checkError()) {
            System.out.println("message: " + message);
            out.println(message);
            out.flush();
        }
    }

    public void stopClient() {
        mRun = false;

    }

    public void run() {

        mRun = true;

        try {
            String SERVER_IP = Util.getDestination(context);
            int SERVER_PORT = Constanst.BROADCAST_PORT;

            //here you must put your computer's IP address.
            InetAddress serverAddr = InetAddress.getByName(SERVER_IP);
            Log.e("TCP SI Client", "SI: Connecting...");

            //create a socket to make the connection with the server
            try {
                socket = new Socket(serverAddr, SERVER_PORT);

                //send the message to the server
                out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);

                //receive the message which the server sends back
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                SocketStatus = true;

                if (service.getmMessageListener() != null) {
                    service.getmMessageListener().onConnected();
                }

                //in this while the client listens for the messages sent by the server
                while (mRun) {
                    String serverMessage = in.readLine();

                    try {
                        if (serverMessage != null && service.getmMessageListener() != null) {
                            Log.e("RESPONSE FROM SERVER", "S: Received Message: '" + serverMessage + "'");

                            //call the method messageReceived from MyActivity class
                            JSONObject jsonObject = new JSONObject(serverMessage);
                            String name = (String) jsonObject.get("name");
                            String action = (String) jsonObject.get("action");
                            String value = (String) jsonObject.get("value");

                            if (name.equalsIgnoreCase(JSONDataUtils.Name.ZONE.toString()) && action.equalsIgnoreCase(JSONDataUtils.Action.Set.toString())) {
                                List<String> zones = Radio.getZone();
                                if (zones.contains(value)) {
                                    Util.SetZone(context, value);
                                }

                            } else if (name.equalsIgnoreCase(JSONDataUtils.Name.CHANNEL.toString()) && action.equalsIgnoreCase(JSONDataUtils.Action.Set.toString())) {
                                List<String> chanels = Radio.getChanel();
                                if (chanels.contains(value)) {
                                    Util.SetChanel(context, value);
                                }
                            } else if (name.equalsIgnoreCase(JSONDataUtils.Name.UID.toString()) && action.equalsIgnoreCase(JSONDataUtils.Action.Set.toString())) {
                                Util.SetUID(context, value);

                            } else if (name.equalsIgnoreCase(JSONDataUtils.Name.INCOMING.toString()) && action.equalsIgnoreCase(JSONDataUtils.Action.Set.toString())) {
                                History.insertHistory(context, value);

                            }

                            service.getmMessageListener().messageReceived(
                                    new BroadCastData(name, action, value, BroadCastDataType.DATA_TYPE_FROM_CLIENT)
                            );
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                Log.e("TCP SI Error", "SI: Error", e);
                e.printStackTrace();

                SocketStatus = false;

                if (service.getmMessageListener() != null) {
                    service.getmMessageListener().onDisconnected();
                }

            }
            //the socket must be closed. It is not possible to reconnect to this socket
            // after it is closed, which means a new socket instance has to be created.

        } catch (Exception e) {
            Log.e("TCP SI Error", "SI: Error", e);

        }
    }

    public interface OnMessageReceived {
        public void messageReceived(BroadCastData data);

        public void onConnected();

        public void onDisconnected();
    }
}
