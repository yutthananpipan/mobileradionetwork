package com.drc.radionetwork;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.drc.radionetwork.utils.ConstanstKeycodeF22F25;
import com.drc.radionetwork.utils.ConstanstKeycodeS88;
import com.drc.radionetwork.utils.ConstanstKeycodeT8;
import com.drc.radionetwork.utils.ShareUtils;
import com.drc.radionetwork.onnetwork.network.NetworkProp;
import com.drc.radionetwork.utils.Util;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Objects;


import static com.drc.radionetwork.AudioPlayer.DATA_INCOMMING_OFF_ACTION;
import static com.drc.radionetwork.AudioPlayer.DATA_INCOMMING_ON_ACTION;
import static com.drc.radionetwork.utils.ShareUtils.KEY_BACK_BUTTON_EXITS;
import static com.drc.radionetwork.utils.ShareUtils.KEY_CHECK_WIFI_STATUS;
import static com.drc.radionetwork.utils.ShareUtils.KEY_STATION_NAME;
import static com.drc.radionetwork.utils.ShareUtils.KEY_USE_VOLUME_BUTTONS_TO_TALK;

public class WalkieActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener, SocketConnectionDialog.SettingListeninng {
    private FragmentsAvailable currentFragment;
    private View map_selected, dialer_selected;
    private RelativeLayout map;
    private Fragment fragment;
    private static WalkieActivity instance;
    private MapFragment mapFragment;
    private SocketConnectionDialog socketConnectionDialog;

    private static final String TAG = WalkieActivity.class.getSimpleName();

    private static final int VOLUME_MIN = 1;
    private static final int VOLUME_MAX = 20;

    private int volume = 0;

    private boolean m_exit;
    private Intent m_serviceIntent;
    private ServiceConnection m_serviceConnection;

    public boolean m_ptt;
    public int m_receivers;

    public String device_incomming = "";
    public String device_id = "";

    private TextView display_number;

    static final boolean isInstanciated() {
        return instance != null;
    }

    private static final int MY_PERMISSIONS_REQUEST_RECORD_AUDIO = 1;
    private static final int MY_PERMISSIONS_REQUEST_PhoneState = 2;
    private static final int MY_PERMISSIONS_REQUEST_Storage = 3;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_SETTINGS = 4;
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 5;
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 6;

    private BroadcastReceiver broadcastReceiver;
    private ArrayList<NetworkProp.NetworkAdpaterInfo> addresses;

    public static WalkieActivity instance() {
        if (instance != null)
            return instance;
        throw new RuntimeException("LinphoneActivity not instantiated yet");
    }

    public WalkieActivity.OnGroupChangeListener groupChangeListener;

    @Override
    public void Connect(String ip) {
        socketConnectionDialog.dismiss();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                WalkieService service = WalkieService.getInstance();
                if(service != null){
                    service.mTcpClient.run();
                }
            }
        });
        thread.start();
    }

    @Override
    public void Disconnected() {
        socketConnectionDialog.dismiss();
        try {
            WalkieService service = WalkieService.getInstance();
            if(service != null){
                service.mTcpClient.socket.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    public void Close() {
        socketConnectionDialog.dismiss();
    }

    public void show(String s) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final ViewGroup viewGroup = findViewById(android.R.id.content);
                Snackbar.make(viewGroup, s, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    public interface OnGroupChangeListener {
        void OnGroupChange();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        socketConnectionDialog = new SocketConnectionDialog(this);

        display_number = (TextView) findViewById(R.id.group_number);

        RelativeLayout topbar = (RelativeLayout) findViewById(R.id.top_bar);
        RelativeLayout dialer = (RelativeLayout) findViewById(R.id.dialer);
        dialer.setOnClickListener(this);

        map_selected = findViewById(R.id.map_select);
        dialer_selected = findViewById(R.id.dialer_select);
        ImageView machine_connect = findViewById(R.id.macine_connect);
        ImageView setting = findViewById(R.id.setting);

        instance = this;
        FragmentsAvailable pendingFragmentTransaction = FragmentsAvailable.UNKNOW;

        currentFragment = FragmentsAvailable.EMPTY;
        if (savedInstanceState == null) {
            changeCurrentFragment(FragmentsAvailable.DIALER, getIntent().getExtras());
        } else {
            currentFragment = (FragmentsAvailable) savedInstanceState.getSerializable("currentFragment");
        }

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (Objects.equals(intent.getAction(), "android.net.conn.CONNECTIVITY_CHANGE")
                        || Objects.equals(intent.getAction(), "android.net.wifi.WIFI_STATE_CHANGED")
                        || intent.getAction().equals("android.net.wifi.STATE_CHANGE")) {
                    //SetIPAddress();

                } else if (intent.getAction().equals(ConstanstKeycodeF22F25.ACTION_PTT_KEY_DOWN)
                        || intent.getAction().equals(ConstanstKeycodeS88.PTT_KEY_DOWN)) { //is it our action1?
                    SetPTTView(true);

                } else if (intent.getAction().equals(ConstanstKeycodeF22F25.ACTION_PTT_KEY_UP)
                        || intent.getAction().equals(ConstanstKeycodeS88.PTT_KEY_UP)) { //is it our action2?
                    SetPTTView(false);

                } else if (intent.getAction().equals(ConstanstKeycodeT8.PTT_KEY)) {
                    try {
                        Bundle bundle = intent.getExtras();
                        assert bundle != null;
                        String key = Objects.requireNonNull(bundle.get("ptt_action")).toString();

                        if (key.equals(ConstanstKeycodeT8.PTT_DOWN)) {
                            SetPTTView(true);

                        } else if (key.equals(ConstanstKeycodeT8.PTT_UP)) {
                            SetPTTView(false);

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (intent.getAction().equals(DATA_INCOMMING_ON_ACTION)) {
                    PTTViewCommingDataIn(true);

                } else if (intent.getAction().equals(DATA_INCOMMING_OFF_ACTION)) {
                    PTTViewCommingDataIn(false);

                } else if (intent.getAction().equals("android.media.VOLUME_CHANGED_ACTION")) {
                    if (currentFragment == FragmentsAvailable.DIALER) {
                        PTTFragment fragment = (PTTFragment) WalkieActivity.this.fragment;
                        fragment.changeVolumeIcon();

                    }
                }
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        filter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        filter.addAction("android.net.wifi.STATE_CHANGE");

        //ptt listening
        filter.addAction(ConstanstKeycodeF22F25.ACTION_PTT_KEY_DOWN);
        filter.addAction(ConstanstKeycodeF22F25.ACTION_PTT_KEY_UP);
        filter.addAction(ConstanstKeycodeS88.PTT_KEY_UP);
        filter.addAction(ConstanstKeycodeS88.PTT_KEY_DOWN);
        filter.addAction(ConstanstKeycodeT8.PTT_KEY);

        //socket listening
        filter.addAction(DATA_INCOMMING_ON_ACTION);
        filter.addAction(DATA_INCOMMING_OFF_ACTION);

        filter.addAction("android.media.VOLUME_CHANGED_ACTION");

        registerReceiver(broadcastReceiver, filter);

        machine_connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });

        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), ClientSettingActivity.class));
            }
        });
    }

    private void openDialog() {
        socketConnectionDialog.setCancelable(false);
        socketConnectionDialog.show(getSupportFragmentManager(), "Setting dialog");
    }

    private void SetIPAddress() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    addresses = NetworkProp.getInstance().getAdaptersAddress();
                    NetworkProp.NetworkAdpaterInfo info = addresses.get(0);
                    display_number.setText(info.getIpAddress());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void SetPTTView(final boolean isptt) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (currentFragment == FragmentsAvailable.DIALER) {
                        PTTFragment fragment = (PTTFragment) WalkieActivity.this.fragment;
                        fragment.getPtt_button().setSelected(isptt);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void PTTViewCommingDataIn(final boolean isDataIncome) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    AudioPlayer audioPlayer = WalkieService.audioPlayer;
                    if (audioPlayer != null) {
                        String IP_INCOME = audioPlayer.IP_INCOME;
                        if (!IP_INCOME.contains("127.0.0.1") && !IP_INCOME.isEmpty()) {
                            display_number.setText((isDataIncome) ? (device_incomming.isEmpty() ? "Radio" : device_incomming) + " is incoming." : device_id);
                            display_number.setTextColor((isDataIncome) ? getResources().getColor(R.color.colorGreen) : getResources().getColor(R.color.colorE));

                            if (currentFragment == FragmentsAvailable.DIALER) {
                                PTTFragment fragment = (PTTFragment) WalkieActivity.this.fragment;
                                fragment.getPtt_button().setActivated(isDataIncome);
                                if (isDataIncome) {
                                    fragment.IC_Ptt().setSelected(false);
                                    fragment.getText_status().setText("PTT Receiving");
                                } else {
                                    fragment.runViewIconPtt();
                                    WalkieService service = WalkieService.getInstance();
                                    if (service != null) {
                                        if(!service.getAudioRecorder().isPtt()){
                                            fragment.setPttSocketConnectStatus(service.mTcpClient.isSocketStatus());
                                        }
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        CheckPermission();
        CheckShellFile();

        if (WalkieService.getInstance() == null) {
            startService(new Intent(getApplicationContext(), WalkieService.class));
        }

        if (TrackingService.getInstance() == null) {
            startService(new Intent(getApplicationContext(), TrackingService.class));
        }

        initDeviceID();

        Log.i(TAG, "onResume");

        final NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancelAll();

        m_exit = false;
        //m_listViewAdapter.clear();

        final SharedPreferences sharedPreferences = getPreferences(Context.MODE_PRIVATE);
        String m_stationName = sharedPreferences.getString(KEY_STATION_NAME, null);
        if ((m_stationName == null) || m_stationName.isEmpty())
            m_stationName = Build.MODEL;

        boolean m_useVolumeButtonsToTalk = sharedPreferences.getBoolean(KEY_USE_VOLUME_BUTTONS_TO_TALK, false);

        if (!m_stationName.isEmpty()) {
            final String title = getString(R.string.app_name) + ": " + m_stationName;
            setTitle(title);
        }

        /*** Check WiFi status */

        final boolean checkWiFiStatus = sharedPreferences.getBoolean(KEY_CHECK_WIFI_STATUS, true);
        if (checkWiFiStatus) {
            final WifiManager manager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
            if (!manager.isWifiEnabled()) {
                final LayoutInflater layoutInflater = LayoutInflater.from(this);
                final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
                final View dialogView = layoutInflater.inflate(R.layout.dialog_wifi, null);
                final DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Log.d(TAG, "WiFiDialog: which=" + which);
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            manager.setWifiEnabled(true);
                        }
                        final CheckBox checkBox = (CheckBox) dialogView.findViewById(R.id.checkBoxNeverAskAgain);
                        if (checkBox.isChecked()) {
                            final SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putBoolean(KEY_CHECK_WIFI_STATUS, false);
                            editor.apply();
                        }
                    }
                };
                dialogBuilder.setView(dialogView);
                dialogBuilder.setPositiveButton(getString(R.string.turn_wifi_on), listener);
                dialogBuilder.setNegativeButton(getString(R.string.cancel), listener);
                final AlertDialog dialog = dialogBuilder.create();
                dialog.show();
            }
        }

        //SetIPAddress();
    }

    public void initDeviceID() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    device_id = Util.getUID(getApplicationContext());
                    display_number.setText(device_id);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void CheckPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.RECORD_AUDIO},
                    MY_PERMISSIONS_REQUEST_RECORD_AUDIO);
            return;
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    MY_PERMISSIONS_REQUEST_PhoneState);
            return;
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_Storage);
            return;
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            return;
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            return;
        }

    }

    private void CheckShellFile() {
        try {
            //red
            WriteFile("open_led_red.sh", "echo 1 > /sys/class/leds/red/brightness");
            WriteFile("close_led_red.sh", "echo 0 > /sys/class/leds/red/brightness");

            //green
            WriteFile("open_led_green.sh", "echo 1 > /sys/class/leds/green/brightness");
            WriteFile("close_led_green.sh", "echo 0 > /sys/class/leds/green/brightness");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void WriteFile(String filename, String content) {
        try {
            File directory = getApplicationContext().getFilesDir();
            File file = new File(directory, filename);
            if (!file.exists()) {

                FileOutputStream outputStream;

                try {
                    outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
                    outputStream.write(content.getBytes());
                    outputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_RECORD_AUDIO: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (WalkieService.getInstance() == null) {
                        startService(new Intent(getApplicationContext(), WalkieService.class));
                    }
                } else {
                    finish();
                }

            }

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "onPause");

    }

    public void changeCurrentFragment(FragmentsAvailable newFragmentType, Bundle extras) {
        changeCurrentFragment(newFragmentType, extras, false);
    }

    private void changeFragment(Fragment newFragment, FragmentsAvailable newFragmentType, boolean withoutAnimation) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();

        if (newFragmentType != FragmentsAvailable.DIALER
                && newFragmentType != FragmentsAvailable.CONTACTS_LIST
                && newFragmentType != FragmentsAvailable.CHAT_LIST
                && newFragmentType != FragmentsAvailable.HISTORY_LIST) {
            transaction.addToBackStack(newFragmentType.toString());
        } else {
            while (fm.getBackStackEntryCount() > 0) {
                fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        }

        transaction.replace(R.id.fragmentContainer, newFragment, newFragmentType.toString());
        transaction.commitAllowingStateLoss();
        fm.executePendingTransactions();

        currentFragment = newFragmentType;

        //SetIPAddress();
    }

    private void changeCurrentFragment(FragmentsAvailable newFragmentType, Bundle extras, boolean withoutAnimation) {

        if (currentFragment == FragmentsAvailable.DIALER) {
            PTTFragment pttFragment = PTTFragment.instance();
            Fragment.SavedState dialerSavedState = getFragmentManager().saveFragmentInstanceState(pttFragment);
        }

        fragment = null;

        switch (newFragmentType) {
            case CONTACTS_LIST:
                fragment = new ContactsListFragment();

                break;
            case DIALER:
                fragment = new PTTFragment();
                break;

            default:
                break;
        }

        if (fragment != null) {
            //this.SetPTT(false);
            fragment.setArguments(extras);
            changeFragment(fragment, newFragmentType, withoutAnimation);
        }
    }

    @Override
    public void onBackPressed() {
        final SharedPreferences sharedPreferences = getPreferences(Context.MODE_PRIVATE);
        m_exit = sharedPreferences.getBoolean(KEY_BACK_BUTTON_EXITS, false);

        super.onBackPressed();
    }

    private void resetSelection() {
        map_selected.setVisibility(View.GONE);
        dialer_selected.setVisibility(View.GONE);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        AudioManager audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);

        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_UP:
            case KeyEvent.KEYCODE_VOLUME_UP:
                audioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC,
                        AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI);
                return true;

            case KeyEvent.KEYCODE_DPAD_DOWN:
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                audioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC,
                        AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI);

                return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        resetSelection();

        if (id == R.id.contacts) {
            changeCurrentFragment(FragmentsAvailable.MAP, null);
            map_selected.setVisibility(View.VISIBLE);
        } else if (id == R.id.dialer) {
            changeCurrentFragment(FragmentsAvailable.DIALER, null);
            dialer_selected.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onDestroy() {
        instance = null;

        unregisterReceiver(broadcastReceiver);
        //this.listener = null;
        //stopService(new Intent(this, WalkieService.class));
        super.onDestroy();
    }

    public MapFragment getMapFragment() {
        return mapFragment;
    }

    public void setMapFragment(MapFragment fragment) {
        this.mapFragment = fragment;
    }

    public void SetPTT(final boolean ispress) {
        if (ispress) {
            Intent i = new Intent();
            i.setAction(ConstanstKeycodeS88.PTT_KEY_DOWN);
            sendBroadcast(i);
        } else {
            Intent i = new Intent();
            i.setAction(ConstanstKeycodeS88.PTT_KEY_UP);
            sendBroadcast(i);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Context context = getApplicationContext();
        SharedPreferences shared = context.getSharedPreferences(ShareUtils.PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = shared.edit();
        edit.putString(ShareUtils.KEY_GROUP_ID, String.valueOf(position));
        edit.apply();

        if (groupChangeListener != null) {
            groupChangeListener.OnGroupChange();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
