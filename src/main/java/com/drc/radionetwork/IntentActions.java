package com.drc.radionetwork;

class IntentActions {

    public static final String KEEP_ALIVE_SERVICE_RECONNECT = "com.android.KEEP_ALIVE_SERVICE_RECONNECT";
    public static final String KEEP_ALIVE_SERVICE_PING_SERVER = "com.android.KEEP_ALIVE_SERVICE_PING_SERVER";
    public static final String KEEP_ALIVE_SERVICE_START = "com.android.KEEP_ALIVE_SERVICE_START";
    public static final String KEEP_ALIVE_SERVICE_STOP = "com.android.KEEP_ALIVE_SERVICE_STOP";
}
