package com.drc.radionetwork;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ApplicationReceiver extends BroadcastReceiver {
    String Log = "ApplicationReceiver";

    public ApplicationReceiver() {
        super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();
        if (action.equals(Intent.ACTION_BOOT_COMPLETED)) {
            if (WalkieService.getInstance() == null) {
                context.startService(new Intent(context, WalkieService.class));
            }

            if (TrackingService.getInstance() == null) {
                context.startService(new Intent(context, TrackingService.class));
            }

        } else if (action.equals(Intent.ACTION_PACKAGE_REPLACED)) {

        }

        android.util.Log.d(Log, action);
    }
}
