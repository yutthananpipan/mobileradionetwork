package com.drc.radionetwork;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.drc.radionetwork.utils.Util;
import com.google.android.material.snackbar.Snackbar;

import java.util.Objects;

public class SocketConnectionDialog extends AppCompatDialogFragment {
    private EditText ip_server;
    private Button connect, disconnect;
    private SettingListeninng listeninng;
    private ImageView close;

    public SocketConnectionDialog(SettingListeninng listeninng) {
        this.listeninng = listeninng;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = Objects.requireNonNull(getActivity()).getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_dialog, null);

        ip_server = view.findViewById(R.id.ip_server);
        connect = view.findViewById(R.id.btnConnect);
        disconnect = view.findViewById(R.id.btnDisconnect);
        close = view.findViewById(R.id.close);
        ip_server.setText(Util.getDestination(getActivity()));

        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ip_server.getText().toString().trim().equals("")) {
                    try {
                        Util.SetDestination(Objects.requireNonNull(getActivity()), ip_server.getText().toString());
                        listeninng.Connect(ip_server.getText().toString());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Snackbar.make(view, "Please insert ip before connect", Snackbar.LENGTH_SHORT).show();
                }
            }
        });

        disconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    listeninng.Disconnected();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    listeninng.Close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        builder.setView(view);

        return builder.create();
    }

    public interface SettingListeninng {
        void Connect(String ip);
        void Disconnected();
        void Close();
    }
}
