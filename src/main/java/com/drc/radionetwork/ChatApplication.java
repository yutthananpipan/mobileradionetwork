package com.drc.radionetwork;

import android.app.Application;

public class ChatApplication extends Application {

    private WalkieService walkieService;

    public WalkieService getWalkieService() {
        return walkieService;
    }

    public void setWalkieService(WalkieService walkieService) {
        this.walkieService = walkieService;
    }
}
