package com.drc.radionetwork;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

import com.drc.radionetwork.utils.ConstanstKeycodeF22F25;
import com.drc.radionetwork.utils.ConstanstKeycodeS88;
import com.drc.radionetwork.utils.ConstanstKeycodeT8;

public class KeyReceiver extends BroadcastReceiver {

    private static final String LOG_TAG = KeyReceiver.class.getSimpleName();

    private static boolean isKey = false;
    private Thread thread = new Thread();

    public KeyReceiver() {

    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
        Log.d(LOG_TAG, intent.getAction().toString());
        try {
            if (intent.getAction().equals(ConstanstKeycodeF22F25.ACTION_PTT_KEY_DOWN)
                    || intent.getAction().equals(ConstanstKeycodeS88.PTT_KEY_DOWN)) { //is it our action1?

                PTT_DOWN(context);

            } else if (intent.getAction().equals(ConstanstKeycodeF22F25.ACTION_PTT_KEY_UP)
                    || intent.getAction().equals(ConstanstKeycodeS88.PTT_KEY_UP)) { //is it our action2?
                PTT_UP(context);
            }

            if (intent.getAction().equals(ConstanstKeycodeT8.PTT_KEY)) {
                try {
                    Bundle bundle = intent.getExtras();
                    String key = bundle.get("ptt_action").toString();

                    if (key.equals(ConstanstKeycodeT8.PTT_DOWN)) {
                        PTT_DOWN(context);
                    } else if (key.equals(ConstanstKeycodeT8.PTT_UP)) {
                        PTT_UP(context);
                    }
                } catch (Exception e) {
                    e.toString();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void PTT_DOWN(Context context) throws Exception {
        Log.d(LOG_TAG, "PTT_DOWN");

        //set code
        if (WalkieService.getInstance() != null) {
            if (WalkieService.m_audioRec != null) {
                WalkieService service = WalkieService.getInstance();
                if (service != null) {
                    service.PlaySound(WalkieService.PTT_STATUS.DOWN);
                }

                WalkieService.m_audioRec.startRecording(true);
                isKey = true;

                Log.d(LOG_TAG, "set audioRecording");

            } else {
                Log.d(LOG_TAG, "audioRecorder = null");
            }
        } else {
            Log.d(LOG_TAG, "WalkieService = null");
        }
    }

    private void PTT_UP(Context context) {
        Log.d(LOG_TAG, "PTT_UP");

        WalkieService service = WalkieService.getInstance();
        if (service != null) {
            service.PlaySound(WalkieService.PTT_STATUS.UP);
        }

        //set code
        if (WalkieService.m_audioRec != null) {
            WalkieService.m_audioRec.stopRecording();
            isKey = false;


        }

    }

    public IntentFilter getIntentFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(ConstanstKeycodeF22F25.ACTION_PTT_KEY_DOWN);
        filter.addAction(ConstanstKeycodeF22F25.ACTION_PTT_KEY_UP);
        filter.addAction(ConstanstKeycodeF22F25.PPTbroadcast);
        filter.addAction(ConstanstKeycodeF22F25.SOSbroadcast);
        filter.addAction(ConstanstKeycodeF22F25.volumeDown);
        filter.addAction(ConstanstKeycodeF22F25.volumeUp);
        filter.addAction(ConstanstKeycodeF22F25.VIDEO_KEYDOWN);
        filter.addAction(ConstanstKeycodeF22F25.VIDEO_KEYUP);
        filter.addAction(ConstanstKeycodeF22F25.KEYCODE_BACK);
        filter.addAction(ConstanstKeycodeF22F25.KEYCODE_CALL);
        filter.addAction(ConstanstKeycodeF22F25.KEYCODE_ENDCALL);
        filter.addAction(ConstanstKeycodeF22F25.KEYCODE_0);
        filter.addAction(ConstanstKeycodeF22F25.KEYCODE_1);
        filter.addAction(ConstanstKeycodeF22F25.KEYCODE_2);
        filter.addAction(ConstanstKeycodeF22F25.KEYCODE_3);
        filter.addAction(ConstanstKeycodeF22F25.KEYCODE_4);
        filter.addAction(ConstanstKeycodeF22F25.KEYCODE_5);
        filter.addAction(ConstanstKeycodeF22F25.KEYCODE_6);
        filter.addAction(ConstanstKeycodeF22F25.KEYCODE_7);
        filter.addAction(ConstanstKeycodeF22F25.KEYCODE_8);
        filter.addAction(ConstanstKeycodeF22F25.KEYCODE_9);
        filter.addAction(ConstanstKeycodeF22F25.KEYCODE_STAR);
        filter.addAction(ConstanstKeycodeF22F25.KEYCODE_POUND);
        filter.addAction(ConstanstKeycodeF22F25.KEYCODE_DPAD_UP);
        filter.addAction(ConstanstKeycodeF22F25.KEYCODE_DPAD_DOWN);
        filter.addAction(ConstanstKeycodeF22F25.KEYCODE_DPAD_LEFT);
        filter.addAction(ConstanstKeycodeF22F25.KEYCODE_DPAD_RIGHT);
        filter.addAction(ConstanstKeycodeF22F25.KEYCODE_DPAD_CENTER);
        filter.addAction(ConstanstKeycodeF22F25.KEYCODE_VOLUME_UP);
        filter.addAction(ConstanstKeycodeF22F25.KEYCODE_VOLUME_DOWN);
        filter.addAction(ConstanstKeycodeF22F25.KEYCODE_POWER);
        filter.addAction(ConstanstKeycodeF22F25.KEYCODE_CAMERA);
        filter.addAction(ConstanstKeycodeF22F25.KEYCODE_MENU);
        filter.addAction(ConstanstKeycodeF22F25.KEYCODE_SOS);
        filter.addAction(ConstanstKeycodeF22F25.KEYCODE_EXT);
        filter.addAction(ConstanstKeycodeF22F25.SOUND_PTT_CALL_DOWN);
        filter.addAction(ConstanstKeycodeF22F25.SOUND_ENDCALL);
        filter.addAction(ConstanstKeycodeF22F25.SOUND_RECEIVECALL);

        filter.addAction(String.valueOf(ConstanstKeycodeS88.back_camera));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.External_camera));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.Front_Camera));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.KEYCODE_0));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.KEYCODE_1));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.KEYCODE_2));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.KEYCODE_3));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.KEYCODE_4));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.KEYCODE_5));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.KEYCODE_6));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.KEYCODE_7));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.KEYCODE_8));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.KEYCODE_9));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.KEYCODE_BACK));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.KEYCODE_CALL));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.KEYCODE_CAMERA));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.KEYCODE_DPAD_CENTER));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.KEYCODE_DPAD_DOWN));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.KEYCODE_DPAD_LEFT));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.KEYCODE_DPAD_RIGHT));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.KEYCODE_DPAD_UP));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.KEYCODE_ENDCALL));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.KEYCODE_EXT));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.KEYCODE_MENU));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.KEYCODE_POWER));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.KEYCODE_POUND));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.KEYCODE_SOS));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.KEYCODE_STAR));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.KEYCODE_VOLUME_DOWN));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.KEYCODE_VOLUME_UP));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.video_KEYDOWN));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.video_KEYUP));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.PTT_KEY_UP));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.PTT_KEY_DOWN));
        filter.addAction(String.valueOf(ConstanstKeycodeS88.SOS_ALARM));
        filter.addAction(String.valueOf(ConstanstKeycodeT8.PTT_KEY));
        filter.addAction(String.valueOf(ConstanstKeycodeT8.F8));
        return filter;
    }

    public static class StatusCallSound {
        static int ptt_down = 0;
        static int ptt_up = 1;
        static int ptt_receive = 2;
        static int ptt_end_receive = 3;

    }

    public static void setIsKey(boolean isKey) {
        KeyReceiver.isKey = isKey;
    }

    public static boolean isKey() {
        return isKey;
    }

}
