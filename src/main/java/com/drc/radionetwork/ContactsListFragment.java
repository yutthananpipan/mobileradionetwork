/*
ContactsListFragment.java
Copyright (C) 2015  Belledonne Communications, Grenoble, France

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package com.drc.radionetwork;

import android.app.Fragment;
import android.os.Bundle;
import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.drc.radionetwork.model.User;
import com.drc.radionetwork.utils.DevicesDatasource;
import com.drc.radionetwork.R;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * @author Sylvain Berfini
 */
public class ContactsListFragment extends Fragment implements WalkieActivity.OnGroupChangeListener {
    private LayoutInflater mInflater;
    private ListViewAdapter m_listViewAdapter;
    private WalkieActivity walkieActivity;
    private TextView textView;
    private ImageView setting;
    private ListView listView;

    public int ItemSelect = -1;
    private boolean isPTT = false;

    @Override
    public void OnGroupChange() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                final DevicesDatasource devicesDatasource = new DevicesDatasource(walkieActivity.getApplicationContext());
                devicesDatasource.getItemFromDummy();

                walkieActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        m_listViewAdapter = new ListViewAdapter(WalkieActivity.instance());
                        listView.setAdapter(m_listViewAdapter);
                        m_listViewAdapter.setStationInfo(devicesDatasource);
                    }
                });
            }
        }).start();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mInflater = inflater;
        View view = inflater.inflate(R.layout.contacts
                , container, false);

        if (WalkieActivity.isInstanciated()) {
            walkieActivity = WalkieActivity.instance();
            //walkieActivity.setContactsListFragment(this);
        }

        textView = (TextView) view.findViewById(R.id.textViewStatus);
        listView = (ListView) view.findViewById(R.id.listView);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();

        if (WalkieActivity.isInstanciated()) {
            walkieActivity.groupChangeListener = this;

            new Thread(new Runnable() {
                @Override
                public void run() {

                    final DevicesDatasource devicesDatasource = new DevicesDatasource(walkieActivity.getApplicationContext());
                    devicesDatasource.getItemFromDummy();

                    walkieActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            m_listViewAdapter = new ListViewAdapter(WalkieActivity.instance());
                            listView.setAdapter(m_listViewAdapter);
                            m_listViewAdapter.setStationInfo(devicesDatasource);
                        }
                    });
                }
            }).start();
        }
    }

    @Override
    public void onDestroy() {
        walkieActivity.groupChangeListener = null;

        if (WalkieService.getInstance() != null) {
            AudioRec recorder = WalkieService.getInstance().getAudioRecorder();

            //set code
            if (recorder != null && ItemSelect != -1) {
                if (WalkieService.getInstance() != null) {
                    WalkieService.getInstance().PlaySound(WalkieService.PTT_STATUS.UP);
                }

                recorder.stopRecording();
                KeyReceiver.setIsKey(false);
            }
        }

        super.onDestroy();
    }

    private class ListViewAdapter extends ArrayAdapter<User> {
        private final WalkieActivity m_activity;
        private final LayoutInflater m_inflater;
        private final StringBuilder m_stringBuilder;

        private DevicesDatasource devicesDatasource;
        private ListViewRow rowView = null;

        public ListViewAdapter(WalkieActivity activity) {
            super(activity, R.layout.list_view_row);
            m_activity = activity;
            m_inflater = (LayoutInflater) activity.getSystemService(LAYOUT_INFLATER_SERVICE);
            m_stringBuilder = new StringBuilder();

            devicesDatasource = new DevicesDatasource(activity);

        }

        public void setStationInfo(DevicesDatasource datasource) {
            this.devicesDatasource = datasource;
            notifyDataSetChanged();
        }

        public int getCount() {
            return devicesDatasource.getCount();
        }

        public User getItem(int position) {
            return devicesDatasource.getItem(position);
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            try {
                rowView = (ListViewRow) convertView;
                if (rowView == null) {
                    rowView = (ListViewRow) m_inflater.inflate(R.layout.list_view_row, null, true);
                    rowView.init(m_activity);
                }

                final User user = getItem(position);

                rowView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return false;
                    }
                });


                rowView.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {

                            if (!isPTT) {
                                isPTT = true;

                                try {
                                    if (WalkieService.getInstance() != null) {

                                        AudioRec recorder = WalkieService.getInstance().getAudioRecorder();
                                        final User stationInfo = m_listViewAdapter.getItem(position);

                                        ItemSelect = position;
                                        //set code

                                        if (recorder != null) {
                                            if (WalkieService.getInstance() != null) {
                                                WalkieService.getInstance().PlaySound(WalkieService.PTT_STATUS.DOWN);
                                            }

                                            recorder.startRecording(true);
                                            KeyReceiver.setIsKey(true);

                                        }

                                    }
                                    rowView.getRelative_row().setBackgroundColor(getResources().getColor(R.color.colorDRC));
                                    m_listViewAdapter.notifyDataSetChanged();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                if (ItemSelect == position) {
                                    isPTT = false;
                                    ItemSelect = -1;

                                    if (WalkieService.getInstance() != null) {
                                        AudioRec recorder = WalkieService.getInstance().getAudioRecorder();

                                        //set code
                                        if (recorder != null) {
                                            if (WalkieService.getInstance() != null) {
                                                WalkieService.getInstance().PlaySound(WalkieService.PTT_STATUS.UP);
                                            }

                                            recorder.stopRecording();
                                            KeyReceiver.setIsKey(false);
                                        }
                                    }
                                }
                            }

                            m_listViewAdapter.notifyDataSetChanged();
                        } catch (Exception e) {
                            e.toString();
                        }
                    }
                });

                rowView.getRelative_row().setBackgroundColor(
                        (ItemSelect == position) ? getResources().getColor(R.color.colorDRC) : getResources().getColor(R.color.background_color)
                );

                rowView.setData(position, user.name, user.IPAddress.toString(), 0);
            } catch (Exception e) {
                e.toString();
            }

            return rowView;
        }
    }

    public void onListViewItemPressed(int position, boolean pressed) {
        try {
            m_listViewAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.toString();
        }
    }
}
