package com.drc.radionetwork.utils;

public class JSONDataUtils {
    public enum Name {
        Connect("connect"),
        StartCall("StartCall"),
        EndCall("EndCall"),
        GPS("GPS"),
        ZONE("ZONE"),
        UID("UID"),
        INCOMING("INCOMING"),
        CHANNEL("CHANNEL");

        private final String data;

        Name(String data) {
            this.data = data;
        }

        @Override
        public String toString() {
            return data;
        }
    }

    public enum Action {
        Get("get"),
        Set("set"),
        Result("Result");

        private final String data;
        Action(String data) {
            this.data = data;
        }

        @Override
        public String toString() {
            return data;
        }
    }

    public enum Value {
        Success("Success"),
        Unsuccessful("Unsuccessful");

        private final String data;
        Value(String data) {
            this.data = data;
        }

        @Override
        public String toString() {
            return data;
        }
    }

}
