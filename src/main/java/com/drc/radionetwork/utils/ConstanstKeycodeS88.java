package com.drc.radionetwork.utils;

import android.provider.BaseColumns;

/**
 * Created by yutthanan on 10/21/16.
 */

public class ConstanstKeycodeS88 implements BaseColumns {

    public static final String PTT_KEY_DOWN =  "android.intent.action.PTT.down";
    public static final String PTT_KEY_UP = "android.intent.action.PTT.up";
    public static final String SOS_ALARM = "com.xigu.soscall.call";
    public static final int Front_Camera=   00;
    public static final int back_camera = 01;
    public static final int External_camera = 02;
    public static final String video_KEYDOWN = "android.hide.videorecorder.down";
    public static final String video_KEYUP = "android.hide.videorecorder.up";
    public static final int KEYCODE_BACK            = 4;
    public static final int KEYCODE_CALL            = 5;
    public static final int KEYCODE_ENDCALL         = 6;
    public static final int KEYCODE_0               = 7;
    public static final int KEYCODE_1               = 8;
    public static final int KEYCODE_2               = 9;
    public static final int KEYCODE_3               = 10;
    public static final int KEYCODE_4               = 11;
    public static final int KEYCODE_5               = 12;
    public static final int KEYCODE_6               = 13;
    public static final int KEYCODE_7               = 14;
    public static final int KEYCODE_8               = 15;
    public static final int KEYCODE_9               = 16;
    public static final int KEYCODE_STAR            = 17;
    public static final int KEYCODE_POUND           = 18;
    public static final int KEYCODE_DPAD_UP         = 19;
    public static final int KEYCODE_DPAD_DOWN       = 20;
    public static final int KEYCODE_DPAD_LEFT       = 21;
    public static final int KEYCODE_DPAD_RIGHT      = 22;
    public static final int KEYCODE_DPAD_CENTER     = 23;
    public static final int KEYCODE_VOLUME_UP       = 24;
    public static final int KEYCODE_VOLUME_DOWN     = 25;
    public static final int KEYCODE_POWER           = 26;
    public static final int KEYCODE_CAMERA          = 27;
    public static final int KEYCODE_MENU            = 82;
    public static final int KEYCODE_SOS        = 229;
    public static final int KEYCODE_EXT        = 230; //PTT

}
