package com.drc.radionetwork.utils;

import android.provider.BaseColumns;

/**
 * Created by yutthanan on 10/21/16.
 */

public class ConstanstKeycodeT8 implements BaseColumns {
    public static final String PTT_UP = "1";
    public static final String PTT_DOWN = "0";

    public static final String PTT_KEY =  "com.android.action.ptt";
    public static final String F8 =  "com.android.action.F8";

}
