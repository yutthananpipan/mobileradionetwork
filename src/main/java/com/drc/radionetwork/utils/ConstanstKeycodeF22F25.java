package com.drc.radionetwork.utils;

import android.provider.BaseColumns;

/**
 * Created by yutthanan on 10/21/16.
 */

public class ConstanstKeycodeF22F25 implements BaseColumns {
    public static final String KEYCODE_BACK = "4";

    public static final String KEYCODE_CALL = "5";

    public static final String KEYCODE_ENDCALL = "6";

    public static final String KEYCODE_0 = "7";

    public static final String KEYCODE_1 = "8";

    public static final String KEYCODE_2 = "9";

    public static final String KEYCODE_3 = "10";

    public static final String KEYCODE_4 = "11";

    public static final String KEYCODE_5 = "12";

    public static final String KEYCODE_6 = "13";

    public static final String KEYCODE_7 = "14";

    public static final String KEYCODE_8 = "15";

    public static final String KEYCODE_9 = "16";

    public static final String KEYCODE_STAR = "17";

    public static final String KEYCODE_POUND = "18";

    public static final String KEYCODE_DPAD_UP = "19";

    public static final String KEYCODE_DPAD_DOWN = "20";

    public static final String KEYCODE_DPAD_LEFT = "21";

    public static final String KEYCODE_DPAD_RIGHT = "22";

    public static final String KEYCODE_DPAD_CENTER = "23";

    public static final String KEYCODE_VOLUME_UP = "24";

    public static final String KEYCODE_VOLUME_DOWN = "25";

    public static final String KEYCODE_POWER = "26";

    public static final String KEYCODE_CAMERA = "27";

    public static final String KEYCODE_MENU = "82";

    public static final String KEYCODE_SOS = "229";

    public static final String KEYCODE_EXT = "230"; //PTT

    public static final String PPTbroadcast = "Intent.ACTION_PTT_KEY_DOWN";
    public static final String ACTION_PTT_KEY_DOWN = "android.intent.action.PTT.down";
    public static final String ACTION_PTT_KEY_UP = "android.intent.action.PTT.up";

    public static final String SOSbroadcast = "android.intent.action.SOS.down";
    public static final String VIDEO_KEYDOWN = "android.intent.action.cameraKey.down";
    public static final String VIDEO_KEYUP = "android.intent.action.cameraKey.up";
    public static final String volumeDown = "android.intent.action.volumeDownKey.down";
    public static final String volumeUp = "android.intent.action.volumeUpKey.down";

    public static final String SOUND_PTT_CALL_DOWN = "com.linphone.PTT_CALL_DOWN";
    public static final String SOUND_ENDCALL = "com.linphone.ENDCALL";
    public static final String SOUND_RECEIVECALL = "com.linphone.RECEIVECALL";

}
