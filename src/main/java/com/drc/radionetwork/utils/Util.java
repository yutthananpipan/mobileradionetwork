package com.drc.radionetwork.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.drc.radionetwork.model.BroadCastData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class Util {
    synchronized public static void LED_Red_Control(Context context, boolean isOpen) {
        try {
            String path = context.getFilesDir().getPath();
            String[] command = (isOpen) ? new String[]{"sh", path + "/open_led_red.sh"} : new String[]{"sh", path + "/close_led_red.sh"};
            String result = executeCommand(command);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    synchronized public static void LED_Green_Control(Context context, boolean isOpen) {
        try {
            String path = context.getFilesDir().getPath();
            String[] command = (isOpen) ? new String[]{"sh", path + "/open_led_green.sh"} : new String[]{"sh", path + "/close_led_green.sh"};
            String result = executeCommand(command);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String convertStringToJson(BroadCastData data) throws JSONException {
        JSONObject object = new JSONObject();
        object.put("name", data.getName());
        object.put("action", data.getAction());
        object.put("value", data.getValue());
        return object.toString();
    }

    synchronized public static String executeCommand(String[] command) {

        StringBuilder output = new StringBuilder();

        Process p;
        try {
            p = Runtime.getRuntime().exec(command);
            p.waitFor();
            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line = "";
            while ((line = reader.readLine()) != null) {
                output.append(line).append("\n");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return output.toString();
    }

    public static String convertDate2String(Date date) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyy HH:mm:ss");
        return format.format(date);
    }

    public static Date convertString2Date(String dateStr) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyy HH:mm:ss");
        try {
            Date date = format.parse(dateStr);
            System.out.println(date);

            return date;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static long getDifferenceTime(Date dEnd, Date dStart){
        long diff = dEnd.getTime() - dStart.getTime();
        long seconds = diff / 1000;
//        long minutes = seconds / 60;
//        long hours = minutes / 60;
//        long days = hours / 24;
        return seconds;
    }

    public static String getDestination(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(ShareUtils.PREF_NAME, MODE_PRIVATE);
        return preferences.getString(ShareUtils.KEY_RADIO_IP, "");
    }

    public static void SetDestination(Context context, String desc) {
        SharedPreferences pref = context.getSharedPreferences(ShareUtils.PREF_NAME, MODE_PRIVATE);
        @SuppressLint("CommitPrefEdits") SharedPreferences.Editor edit = pref.edit();
        edit.putString(ShareUtils.KEY_RADIO_IP, desc);
        edit.apply();
    }

    public static String getZone(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(ShareUtils.PREF_NAME, MODE_PRIVATE);
        return preferences.getString(ShareUtils.KEY_ZONE_ID, "");
    }

    public static void SetZone(Context context, String zone_id) {
        SharedPreferences pref = context.getSharedPreferences(ShareUtils.PREF_NAME, MODE_PRIVATE);
        @SuppressLint("CommitPrefEdits") SharedPreferences.Editor edit = pref.edit();
        edit.putString(ShareUtils.KEY_ZONE_ID, zone_id);
        edit.apply();
    }

    public static String getChanel(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(ShareUtils.PREF_NAME, MODE_PRIVATE);
        return preferences.getString(ShareUtils.KEY_CHANEL_ID, "");
    }

    public static void SetChanel(Context context, String chanel_id) {
        SharedPreferences pref = context.getSharedPreferences(ShareUtils.PREF_NAME, MODE_PRIVATE);
        @SuppressLint("CommitPrefEdits") SharedPreferences.Editor edit = pref.edit();
        edit.putString(ShareUtils.KEY_CHANEL_ID, chanel_id);
        edit.apply();
    }

    public static String getUID(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(ShareUtils.PREF_NAME, MODE_PRIVATE);
        return preferences.getString(ShareUtils.KEY_UID, "Unknown");
    }

    public static void SetUID(Context context, String id) {
        SharedPreferences pref = context.getSharedPreferences(ShareUtils.PREF_NAME, MODE_PRIVATE);
        @SuppressLint("CommitPrefEdits") SharedPreferences.Editor edit = pref.edit();
        edit.putString(ShareUtils.KEY_UID, id);
        edit.apply();
    }

    public static String getSendID(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(ShareUtils.PREF_NAME, MODE_PRIVATE);
        return preferences.getString(ShareUtils.KEY_SEND_ID, "");
    }

    public static void SetSendID(Context context, String id) {
        SharedPreferences pref = context.getSharedPreferences(ShareUtils.PREF_NAME, MODE_PRIVATE);
        @SuppressLint("CommitPrefEdits") SharedPreferences.Editor edit = pref.edit();
        edit.putString(ShareUtils.KEY_SEND_ID, id);
        edit.apply();
    }

    public static int getVolume(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(ShareUtils.PREF_NAME, MODE_PRIVATE);
        return preferences.getInt(ShareUtils.KEY_VOLUME, 20);
    }

    public static void SetVolume(Context context, int volume) {
        SharedPreferences pref = context.getSharedPreferences(ShareUtils.PREF_NAME, MODE_PRIVATE);
        @SuppressLint("CommitPrefEdits") SharedPreferences.Editor edit = pref.edit();
        edit.putInt(ShareUtils.KEY_VOLUME, volume);
        edit.apply();
    }

    public static String getDateTime(Context context) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.US);
        return df.format(new Date());
    }
}
