package com.drc.radionetwork.utils;

public class ShareUtils {
    public static final String PREF_NAME = "RadioNetwork";
    public static final String KEY_VOLUME = "VOLUME";
    public static final String KEY_GROUP_ID = "GROUP_ID";
    public static final String KEY_ZONE_ID = "ZONE_ID";
    public static final String KEY_CHANEL_ID = "CHANEL_ID";
    public static final String KEY_UID = "RADIO_ID";
    public static final String KEY_SEND_ID = "SEND_RADIO_ID";
    public static final String KEY_RADIO_IP = "RADIO_IP";

    public static final String KEY_STATION_NAME = "station_name";
    public static final String KEY_CHECK_WIFI_STATUS = "check-wifi-status";
    public static final String KEY_USE_VOLUME_BUTTONS_TO_TALK = "use-volume-buttons-to-talk";
    public static final String KEY_BACK_BUTTON_EXITS = "back-button-exits";
}
