package com.drc.radionetwork.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.drc.radionetwork.WalkieService;
import com.drc.radionetwork.model.Group;
import com.drc.radionetwork.model.User;

import java.util.ArrayList;

public class DevicesDatasource {
    private static DevicesDatasource instance;
    private Context context;
    private ArrayList<User> deviceArrayList = new ArrayList<>();

    public DevicesDatasource(Context context) {
        this.context = context;
    }

    public void getItemFromDummy() {
        deviceArrayList.clear();

        SharedPreferences shared = context.getSharedPreferences(ShareUtils.PREF_NAME, Context.MODE_PRIVATE);
        String groupid = shared.getString(com.drc.radionetwork.utils.ShareUtils.KEY_GROUP_ID, "0");
        ArrayList<User> users = new ArrayList<>();

        for(Group group : WalkieService.getGroup()){
            assert groupid != null;
            if(groupid.equals(group.GroupID)){
                try {
                    deviceArrayList.add(group.user);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public User getItem(int position) {
        return deviceArrayList.get(position);
    }

    public int getCount() {
        return deviceArrayList.size();
    }

}
