package com.drc.radionetwork;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.drc.radionetwork.model.Group;

import java.util.List;

public class CustomGroupAdapter extends ArrayAdapter<Group> {

    private final List<Group> data;

    public CustomGroupAdapter(Context context, List<Group> data) {
        super(context, 0, data);
        this.data = data;
    }

    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView,  ViewGroup parent) {
        Group group = data.get(position);
        if(convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_group, parent, false);
            convertView.setTag(ViewHolder.createViewHolder(convertView));
        }
        ViewHolder holder = (ViewHolder)convertView.getTag();
        holder.textCountry.setText(group.GroupName);
        return convertView;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    private static class ViewHolder {
        public ImageView imgFlag;
        public TextView textCountry;

        public static ViewHolder createViewHolder(View view) {
            ViewHolder holder = new ViewHolder();
            holder.imgFlag = (ImageView) view.findViewById(R.id.imgFlag);
            holder.textCountry = (TextView)view.findViewById(R.id.textName);
            return holder;
        }
    }
}
