package com.drc.radionetwork;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import android.os.Handler;
import android.os.Looper;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.drc.radionetwork.model.BroadCastData;
import com.drc.radionetwork.model.Items;
import com.drc.radionetwork.model.Radio;
import com.drc.radionetwork.onnetwork.broadcast.BroadCastDataType;
import com.drc.radionetwork.utils.JSONDataUtils;
import com.drc.radionetwork.utils.Util;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.AUDIO_SERVICE;

/**
 * Created by sammy on 04-Mar-16.
 */
public class PTTFragment extends Fragment implements TCPClient.OnMessageReceived, View.OnClickListener, MaterialSpinner.OnItemSelectedListener, MaterialSpinner.OnNothingSelectedListener {

    private String TAG = PTTFragment.class.getSimpleName();
    private static PTTFragment instance;
    private RelativeLayout ptt_button;
    private ImageView ic_ptt, speaker, mute, lock;
    private ImageButton speaker_volume;
    private TextView text_status;
    private MaterialSpinner channel_spinner, zone_spinner;
    private MaterialSpinner uid_spinner;
    private Button history;

    private boolean isLock = false;

    private WalkieActivity walkieActivity;

    public RelativeLayout getPtt_button() {
        return ptt_button;
    }

    public TextView getText_status() {
        return text_status;
    }

    public ImageView IC_Ptt() {
        return this.ic_ptt;
    }

    public ImageView getSpeaker() {
        return speaker;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.ptt_layout, container, false);
        ptt_button = view.findViewById(R.id.ptt);
        ic_ptt = view.findViewById(R.id.ic_ptt);
        text_status = view.findViewById(R.id.text_status);
        speaker = view.findViewById(R.id.speaker_icon);
        mute = view.findViewById(R.id.mute_icon);
        lock = view.findViewById(R.id.lock_icon);
        speaker_volume = view.findViewById(R.id.speaker_volume);
        history = view.findViewById(R.id.history);

        if (WalkieActivity.isInstanciated()) {
            walkieActivity = WalkieActivity.instance();
        }

        uid_spinner = view.findViewById(R.id.user_spinner);
        channel_spinner = view.findViewById(R.id.channel_spinner);
        zone_spinner = view.findViewById(R.id.zone_spinner);

        uid_spinner.setOnClickListener(this);
        channel_spinner.setOnClickListener(this);
        zone_spinner.setOnClickListener(this);
        speaker_volume.setOnClickListener(this);
        mute.setOnClickListener(this);

        lock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isLock = !isLock;
                lock.setSelected(isLock);

            }
        });

        uid_spinner.setOnItemSelectedListener(this);
        channel_spinner.setOnItemSelectedListener(this);
        zone_spinner.setOnItemSelectedListener(this);

        uid_spinner.setOnNothingSelectedListener(this);
        channel_spinner.setOnNothingSelectedListener(this);
        zone_spinner.setOnNothingSelectedListener(this);

        ptt_button.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                WalkieService service = WalkieService.getInstance();

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:

                        if (service != null) {
                            if (service.getAudioRecorder().isMute()) {
                                Vibrator vib = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
                                // Vibrate for 500 milliseconds
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                    vib.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
                                } else {
                                    //deprecated in API 26
                                    vib.vibrate(500);
                                }

                                //Toast.makeText(walkieActivity.getApplicationContext(), "ไมโครโฟนกำลังปิดอยู่", Toast.LENGTH_LONG).show();
                                walkieActivity.show("Microphone is closing.");

                                return true;
                            }
                        }

                        if (!isLock) {
                            ptt_button.setSelected(true);
                            walkieActivity.SetPTT(true);
                            text_status.setText("PTT Transmitting");
                            if (service != null) {
                                try {
                                    service.SendMessage(Util.convertStringToJson(new BroadCastData(JSONDataUtils.Name.StartCall.toString(), JSONDataUtils.Action.Set.toString(), "", BroadCastDataType.DATA_TYPE_FROM_CLIENT)));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                        } else {
                            validateLockMode();
                        }

                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        ptt_button.setSelected(false);
                        walkieActivity.SetPTT(false);
                        if (service != null) {
                            try {
                                setPttSocketConnectStatus(service.mTcpClient.isSocketStatus());
                                service.SendMessage(Util.convertStringToJson(new BroadCastData(JSONDataUtils.Name.EndCall.toString(), JSONDataUtils.Action.Set.toString(), "", BroadCastDataType.DATA_TYPE_FROM_CLIENT)));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        runViewIconPtt();

                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        ptt_button.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                return false;
            }
        });

        history.setOnClickListener(this);

        instance = this;
        return view;
    }

    private void initData() {
        walkieActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {

                    List<String> zones = Radio.getZone();
                    List<String> chanels = Radio.getChanel();
                    List<String> uids = Radio.getUID();

                    zone_spinner.setItems(zones);
                    channel_spinner.setItems(chanels);
                    uid_spinner.setItems(uids.toArray());

                    String zone = Util.getZone(getActivity());
                    String chanel = Util.getChanel(getActivity());

                    String send_id = Util.getSendID(getActivity());

                    if (zone.isEmpty()) {
                        zone_spinner.setSelectedIndex(0);
                    } else {
                        zone_spinner.setSelectedIndex((zones.contains(zone)) ? zones.indexOf(zone) : 0);
                    }

                    if (chanel.isEmpty()) {
                        channel_spinner.setSelectedIndex(0);
                    } else {
                        channel_spinner.setSelectedIndex((chanels.contains(chanel)) ? chanels.indexOf(chanel) : 0);
                    }

                    if (send_id.isEmpty()) {
                        uid_spinner.setSelectedIndex(0);
                    } else {
                        uid_spinner.setSelectedIndex((uids.contains(send_id)) ? uids.indexOf(send_id) : 0);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private List<Items> getListUserID() {
        ArrayList<Items> list = new ArrayList<>();
        list.add(new Items("Broadcast", false));

        list.add(new Items("Create", true));
        return list;
    }

    public static PTTFragment instance() {
        return instance;
    }

    @Override
    public void onPause() {
        instance = null;
        super.onPause();

    }

    @Override
    public void onResume() {
        super.onResume();
        instance = this;
        ptt_button.setSelected(false);

        getDataFromRadio();

        initData();
        runViewPttStateIcon();
        changeVolumeIcon();

    }

    private void getDataFromRadio() {
        try {
            WalkieService service = WalkieService.getInstance();
            if (service != null && service.mTcpClient != null) {
                service.SendMessage(Util.convertStringToJson(
                        new BroadCastData(JSONDataUtils.Name.ZONE.toString(), JSONDataUtils.Action.Get.toString(), "", BroadCastDataType.DATA_TYPE_FROM_CLIENT))
                );
                service.SendMessage(Util.convertStringToJson(
                        new BroadCastData(JSONDataUtils.Name.CHANNEL.toString(), JSONDataUtils.Action.Get.toString(), "", BroadCastDataType.DATA_TYPE_FROM_CLIENT))
                );
                service.SendMessage(Util.convertStringToJson(
                        new BroadCastData(JSONDataUtils.Name.UID.toString(), JSONDataUtils.Action.Get.toString(), "", BroadCastDataType.DATA_TYPE_FROM_CLIENT))
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void runViewIconPtt() {
        WalkieService service = WalkieService.getInstance();
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ic_ptt.setSelected(!service.getAudioRecorder().isMute() && service.mTcpClient.isSocketStatus());

            }
        });
    }

    private void runViewPttStateIcon() {
        WalkieService service = WalkieService.getInstance();
        if (service != null) {
            if (service.mTcpClient != null) {
                service.setmMessageListener(PTTFragment.this);
                setPttSocketConnectStatus(service.mTcpClient.isSocketStatus());
                if (!service.mTcpClient.isSocketStatus()) {
                    Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            WalkieService service = WalkieService.getInstance();
                            if (service != null) {
                                service.mTcpClient.run();
                            }
                        }
                    });
                    thread.start();
                }
            }
        } else {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    WalkieService service = WalkieService.getInstance();
                    service.setmMessageListener(PTTFragment.this);
                    setPttSocketConnectStatus(service.mTcpClient.isSocketStatus());
                }
            }, 10000);
        }
    }

    public void setPttSocketConnectStatus(boolean ptt_st) {
        walkieActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                text_status.setText(ptt_st ? "PTT Standby" : "Disconnected");
                runViewIconPtt();
            }
        });
    }

    public void changeVolumeIcon() {
        walkieActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    AudioManager am = (AudioManager) walkieActivity.getSystemService(AUDIO_SERVICE);
                    int volume_level = am.getStreamVolume(AudioManager.STREAM_MUSIC);
                    if (volume_level > 8) {
                        speaker.setImageResource(R.drawable.speaker_full);
                    } else if (volume_level > 0) {
                        speaker.setImageResource(R.drawable.speaker_medium);
                    } else {
                        speaker.setImageResource(R.drawable.speaker_mute);
                    }
                    Log.d(TAG, String.valueOf(volume_level));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void validateLockMode() {
        walkieActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isLock) {
                    //Toast.makeText(walkieActivity.getApplicationContext(), "Application is on lock mode.", Toast.LENGTH_LONG).show();
                    walkieActivity.show("Application is on lock mode.");
                    Vibrator vib = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
                    // Vibrate for 500 milliseconds
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        vib.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
                    } else {
                        //deprecated in API 26
                        vib.vibrate(500);
                    }
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (WalkieService.getInstance() != null) {
            WalkieService.getInstance().setmMessageListener(null);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void messageReceived(BroadCastData data) {
        Log.e("RESPONSE FROM SERVER", "S: Received Message: '" + data.getName() + "'");

        try {
            String name = data.getName();
            String action = data.getAction();
            String value = data.getValue();

            if (name.equalsIgnoreCase(JSONDataUtils.Name.UID.toString()) && action.equalsIgnoreCase(JSONDataUtils.Action.Set.toString())) {
                Util.SetUID(getActivity(), value);
                walkieActivity.initDeviceID();

            } else if (name.equalsIgnoreCase(JSONDataUtils.Name.INCOMING.toString()) && action.equalsIgnoreCase(JSONDataUtils.Action.Set.toString())) {
                walkieActivity.device_incomming = value;
            }
            initData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnected() {
        setPttSocketConnectStatus(true);
        getDataFromRadio();
    }

    @Override
    public void onDisconnected() {
        setPttSocketConnectStatus(false);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View v) {
        int id = v.getId();
        validateLockMode();

        if (!isLock) {
            try {
                if (id == R.id.user_spinner) {
                    uid_spinner.setBackgroundColor(getResources().getColor(R.color.background_color));

                } else if (id == R.id.channel_spinner) {
                    channel_spinner.setBackgroundColor(getResources().getColor(R.color.background_color));

                } else if (id == R.id.zone_spinner) {
                    zone_spinner.setBackgroundColor(getResources().getColor(R.color.background_color));

                } else if (id == R.id.speaker_volume) {
                    AudioManager audio = (AudioManager) getActivity().getSystemService(AUDIO_SERVICE);
                    audio.adjustStreamVolume(AudioManager.STREAM_MUSIC,
                            AudioManager.ADJUST_SAME,
                            AudioManager.FLAG_SHOW_UI);

                } else if (id == R.id.mute_icon) {
                    WalkieService service = WalkieService.getInstance();
                    service.getAudioRecorder().setMute(!service.getAudioRecorder().isMute());
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mute.setSelected(service.getAudioRecorder().isMute());
                            runViewIconPtt();

                        }
                    });

                } else if (id == R.id.history) {
                    startActivity(new Intent(walkieActivity.getApplicationContext(), HistoryActivity.class));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            zone_spinner.collapse();
            channel_spinner.collapse();
            uid_spinner.collapse();
        }

    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
        try {
            WalkieService service = WalkieService.getInstance();

            if (view.getId() == R.id.user_spinner) {

                uid_spinner.setBackgroundColor(getResources().getColor(R.color.transparent));

                validateLockMode();
                if (!isLock) {
                    String result = uid_spinner.getText().toString();
                    service.SendMessage(Util.convertStringToJson(new BroadCastData(JSONDataUtils.Name.UID.toString(), JSONDataUtils.Action.Set.toString(), result, BroadCastDataType.DATA_TYPE_FROM_CLIENT)));
                    Util.SetSendID(walkieActivity.getApplicationContext(), result);
                }
            } else if (view.getId() == R.id.channel_spinner) {

                channel_spinner.setBackgroundColor(getResources().getColor(R.color.transparent));

                validateLockMode();
                if (!isLock) {
                    String result = channel_spinner.getText().toString();
                    Util.SetChanel(getActivity(), result);
                    service.SendMessage(Util.convertStringToJson(new BroadCastData(JSONDataUtils.Name.CHANNEL.toString(), JSONDataUtils.Action.Set.toString(), result, BroadCastDataType.DATA_TYPE_FROM_CLIENT)));
                }
            } else if (view.getId() == R.id.zone_spinner) {
                zone_spinner.setBackgroundColor(getResources().getColor(R.color.transparent));

                validateLockMode();
                if (!isLock) {
                    String result = zone_spinner.getText().toString();
                    Util.SetZone(getActivity(), result);
                    service.SendMessage(Util.convertStringToJson(new BroadCastData(JSONDataUtils.Name.ZONE.toString(), JSONDataUtils.Action.Set.toString(), result, BroadCastDataType.DATA_TYPE_FROM_CLIENT)));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onNothingSelected(MaterialSpinner spinner) {
        uid_spinner.setBackgroundColor(getResources().getColor(R.color.transparent));
        channel_spinner.setBackgroundColor(getResources().getColor(R.color.transparent));
        zone_spinner.setBackgroundColor(getResources().getColor(R.color.transparent));
    }
}
