/*
 * Copyright (C) 2015 Sergey Zubarev, info@js-labs.org
 *
 * This file is a part of WiFi WalkieTalkie application.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.drc.radionetwork;

import android.content.Intent;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.drc.radionetwork.utils.Constanst;
import com.drc.radionetwork.utils.Util;
import com.drc.radionetwork.onnetwork.network.NetworkProp;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.ArrayList;

public class AudioPlayer extends AsyncTask<Void, Integer, Void> {
    private static final String LOG_TAG = AudioPlayer.class.getSimpleName();
    private static final String TAG = "AudioPlayer";
    public String IP_INCOME = "";

    private final Handler handler = new Handler(Looper.getMainLooper());

    public boolean isDataComing = false;
    private int count_led_progress = 0;

    public static final String DATA_INCOMMING_ON_ACTION = "com.drc.action_on";
    public static final String DATA_INCOMMING_OFF_ACTION = "com.drc.action_off";

    // 1 or 2
    static final int NUM_CHANNELS = 1;

    static int stream() {
        return AudioManager.STREAM_MUSIC;
    }

    public AudioPlayer(WalkieService service) {

    }

    public void playStream() {
        handler.post(runnable);
        doInBackground();

    }

    private final Runnable runnable = new Runnable() {
        @Override
        public void run() {
            WalkieService service = WalkieService.getInstance();
            if (service != null) {
                Intent i = new Intent();
                i.setAction((isDataComing) ? DATA_INCOMMING_ON_ACTION : DATA_INCOMMING_OFF_ACTION);
                service.getApplicationContext().sendBroadcast(i);

                Util.LED_Green_Control(service.getApplicationContext(), isDataComing);

            }

            if (!isDataComing && !KeyReceiver.isKey()) {
                assert service != null;
                Util.LED_Green_Control(service.getApplicationContext(), count_led_progress == 5);

                if (count_led_progress > 5) {
                    count_led_progress = 0;
                }

                count_led_progress++;
            }

            isDataComing = false;
            handler.postDelayed(this, 1000);
        }
    };

    @Override
    protected Void doInBackground(Void... voids) {

        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
        byte[] outBuf = new byte[AudioRec.FRAME_SIZE];

        DatagramSocket socket;
        DatagramPacket packet;

        try {
            socket = new DatagramSocket(Constanst.AUDIO_PORT);
            packet = new DatagramPacket(outBuf, outBuf.length);

            // Audio track object
            AudioTrack audioTrack = create();
            assert audioTrack != null;

            while (true) {
                try {
                    socket.receive(packet);

                    Log.v(TAG, "Data In");

                    audioTrack.play();

                    ArrayList<NetworkProp.NetworkAdpaterInfo> addresses = WalkieService.getInstance().getAudioRecorder().getAddresses();
                    NetworkProp.NetworkAdpaterInfo info = addresses.get(0);
                    if (!(("/" + info.getIpAddress().trim().toString()).equals(packet.getAddress().toString()))) {
                        isDataComing = true;
                        count_led_progress = 0;

                        IP_INCOME = packet.getAddress().toString();
                        if (IP_INCOME.contains("127.0.0.1")) {
                            continue;
                        }

                        int len = audioTrack.write(outBuf, 0, outBuf.length);
                        Log.v(TAG, "Decoded back " + len * NUM_CHANNELS * 2 + " bytes");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static AudioTrack create() {
        try {
            int minBufSize = AudioRecord.getMinBufferSize(AudioRec.SAMPLE_RATE,
                    AudioFormat.CHANNEL_IN_MONO,
                    AudioFormat.ENCODING_PCM_16BIT);

            // init audio track

            return new AudioTrack(stream(),
                    AudioRec.SAMPLE_RATE,
                    AudioFormat.CHANNEL_OUT_MONO,
                    AudioFormat.ENCODING_PCM_16BIT,
                    minBufSize,

                    AudioTrack.MODE_STREAM);
        } catch (final NumberFormatException ex) {
            Log.e(LOG_TAG, ex.toString());
        }
        return null;
    }

}
