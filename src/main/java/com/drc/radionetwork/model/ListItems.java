package com.drc.radionetwork.model;

public class ListItems {
    private String Channel;
    private String Name;
    private String UID;
    public boolean isHeader;

    public ListItems(String channel, String name, boolean isHeader) {
        Channel = channel;
        Name = name;
        this.isHeader = isHeader;
    }

    public ListItems(String channel, String name, String UID, boolean isHeader) {
        Channel = channel;
        Name = name;
        this.UID = UID;
        this.isHeader = isHeader;
    }

    public String getChannel() {
        return Channel;
    }

    public void setChannel(String channel) {
        Channel = channel;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public boolean isHeader() {
        return isHeader;
    }

    public void setHeader(boolean header) {
        isHeader = header;
    }
}
