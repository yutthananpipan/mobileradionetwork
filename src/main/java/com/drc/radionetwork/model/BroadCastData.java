package com.drc.radionetwork.model;

import com.drc.radionetwork.onnetwork.broadcast.BroadCastDataType;
import com.google.gson.annotations.SerializedName;

public class BroadCastData {

    @SerializedName("value")
    private final String value;

    @SerializedName("name")
    private final String name;

    @SerializedName("action")
    private final String action;

    @SerializedName("type")
    private final String type;

    public BroadCastData(String name, String action, String value, BroadCastDataType type) {
        this.name = name;
        this.action = action;
        this.value = value;
        this.type = type.toString();
    }

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    public String getAction() {
        return action;
    }

    public String getType() {
        return type;
    }
}
