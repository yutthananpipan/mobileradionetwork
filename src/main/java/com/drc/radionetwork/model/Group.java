package com.drc.radionetwork.model;

public class Group {
    public String GroupID;
    public String GroupName;
    public User user;
    public String UserID;

    public static final String GROPU_ALL = "0";
    public static final String GROPU_TEAM_1 = "1";
    public static final String GROPU_TEAM_2= "2";
    public static final String GROPU_TEAM_3 = "3";
    public static final String GROPU_TEAM_4 = "4";
    public static final String GROPU_TEAM_5 = "5";


    public Group(String groupid, String GroupName, User user) {
        this.GroupID = groupid;
        this.user = user;
        this.GroupName = GroupName;
    }
}
