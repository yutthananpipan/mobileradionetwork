package com.drc.radionetwork.model;

public class User {
    public String ID;
    public String IPAddress;
    public int port;
    public String name;
    public String UserName;
    public String Password;

    public User(String ID, String IPAddress, int port, String name) {
        this.ID = ID;
        this.IPAddress = IPAddress;
        this.port = port;
        this.name = name;
    }
}
