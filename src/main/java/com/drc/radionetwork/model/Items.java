package com.drc.radionetwork.model;

public class Items {
    public String Name;
    public boolean isButton;

    public Items(String name, boolean isButton) {
        Name = name;
        this.isButton = isButton;
    }
}
