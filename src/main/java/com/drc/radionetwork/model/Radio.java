package com.drc.radionetwork.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Radio {

    public static List<String> getChanel() {
        return Arrays.asList("CH1", "CH2", "CH3", "CH4", "CH5", "CH6", "CH7", "CH8", "CH9", "CH10",
                "CH11", "CH12", "CH13", "CH14", "CH15", "CH16", "CH17", "CH18", "CH19", "CH20",
                "CH21", "CH22", "CH23", "CH24", "CH25", "CH26", "CH27", "CH28", "CH29", "CH30");
    }

    public static List<String> getZone() {
        return Arrays.asList("ZONE1", "ZONE2", "ZONE3", "ZONE4", "ZONE5", "ZONE6", "ZONE7", "ZONE8", "ZONE9", "ZONE10",
                "ZONE11", "ZONE12", "ZONE13", "ZONE14", "ZONE15", "ZONE16", "ZONE17", "ZONE18", "ZONE19", "ZONE20",
                "ZONE21", "ZONE22", "ZONE23", "ZONE24", "ZONE25", "ZONE26", "ZONE27", "ZONE28", "ZONE29", "ZONE30");
    }

    public static List<String> getUID() {
        return Arrays.asList("Broadcast", "UID1", "UID2", "UID3", "UID4", "UID5", "UID6", "UID7", "UID8", "UID9", "UID10");
    }

}
