package com.drc.radionetwork.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.drc.radionetwork.database.DBContentProvider;
import com.drc.radionetwork.database.HISTORY_TABLE;
import com.drc.radionetwork.utils.Util;

import java.util.ArrayList;
import java.util.Calendar;

public class History {
    private String datetime;
    private String Zone;
    private String Channel;
    private String ID;

    public History() {
    }

    public String getDatetime() {
        return datetime;
    }

    public static ArrayList<History> getHistories(Context context) {
        ArrayList<History> datas = new ArrayList<>();
        Cursor cursor = context.getContentResolver().query(DBContentProvider.HISTORY_CONTENT_URI, null, null, null, HISTORY_TABLE.DATETIME + " DESC LIMIT 50");
        try {
            if (cursor != null) {
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    do {
                        History history = new History();
                        history.setZone(cursor.getString(cursor.getColumnIndex(HISTORY_TABLE.ZONE)));
                        history.setChannel(cursor.getString(cursor.getColumnIndex(HISTORY_TABLE.CHANEL)));
                        history.setID(cursor.getString(cursor.getColumnIndex(HISTORY_TABLE.RADIO_ID)));
                        history.setDatetime(cursor.getString(cursor.getColumnIndex(HISTORY_TABLE.DATETIME)));

                        datas.add(history);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (cursor != null) cursor.close();

        return datas;
    }

    public static void insertHistory(Context context, String uid) {
        try {
            ContentValues value = new ContentValues();
            value.put(HISTORY_TABLE.ZONE, Util.getZone(context));
            value.put(HISTORY_TABLE.CHANEL, Util.getChanel(context));
            value.put(HISTORY_TABLE.RADIO_ID, uid);
            value.put(HISTORY_TABLE.DATETIME, Util.convertDate2String(Calendar.getInstance().getTime()));

            context.getContentResolver().insert(DBContentProvider.HISTORY_CONTENT_URI, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getZone() {
        return Zone;
    }

    public void setZone(String zone) {
        Zone = zone;
    }

    public String getChannel() {
        return Channel;
    }

    public void setChannel(String channel) {
        Channel = channel;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }
}
