package com.drc.radionetwork;

import android.os.Process;
import android.util.Log;

import com.drc.radionetwork.onnetwork.network.NetworkProp;
import com.drc.radionetwork.utils.Constanst;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.ArrayList;
import java.util.Arrays;

public class TransferSocket implements Runnable {
    private static final String LOG_TAG = TransferSocket.class.getSimpleName();

    private final Thread m_thread;
    private DatagramSocket socket;
    private DatagramPacket packet;

    private int PORT;
    String dest;

    public TransferSocket(int port, String destlist, String id) {
        this.PORT = port;
        this.dest = destlist;

        m_thread = new Thread(this, LOG_TAG + id);
        m_thread.start();
    }

    @Override
    public void run() {
        android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_URGENT_AUDIO);

        byte[] encBuf = new byte[AudioRec.FRAME_SIZE];
        try {
            socket = new MulticastSocket(PORT);
            packet = new DatagramPacket(encBuf, encBuf.length);

        } catch (IOException e) {
            e.printStackTrace();
        }

        while (true) {
            try {
                socket.receive(packet);

                Log.v(LOG_TAG, "Data In");

                ArrayList<NetworkProp.NetworkAdpaterInfo> addresses = WalkieService.getInstance().getAudioRecorder().getAddresses();
                NetworkProp.NetworkAdpaterInfo info = addresses.get(0);
                if (!("/" + info.getIpAddress().trim().toString()).equals(packet.getAddress().toString())) {

                    byte[] encBuf2 = Arrays.copyOf(encBuf, packet.getLength());

                    DatagramPacket packet1 = new DatagramPacket(encBuf2, encBuf2.length, InetAddress.getByName(dest), Constanst.AUDIO_PORT);
                    socket.send(packet1);

                }

            } catch (Exception e) {
                e.toString();
            }
        }
    }
}
