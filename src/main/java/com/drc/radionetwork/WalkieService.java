/*
 * Copyright (C) 2015 Sergey Zubarev, info@js-labs.org
 *
 * This file is a part of WiFi WalkieTalkie application.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.drc.radionetwork;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.provider.Settings;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import android.util.Base64;
import android.util.Log;

import com.drc.radionetwork.model.Group;
import com.drc.radionetwork.model.User;
import com.drc.radionetwork.utils.JSONDataUtils;
import com.vilyever.socketclient.SocketClient;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Random;

public class WalkieService extends Service {
    private static final String TAG = WalkieService.class.getSimpleName();

    private TCPClient.OnMessageReceived mMessageListener = null;

    private KeyReceiver keyReceiver;
    public static AudioPlayer audioPlayer;
    public static AudioRec m_audioRec;

    public TCPClient mTcpClient = null;
    private connectTask conctTask = null;
    private SocketClient socketClient;

    private static WalkieService instance;
    public final static int MAX_VOLUME = 100;

    public static WalkieService getInstance() {
        return instance;
    }

    public void setmMessageListener(TCPClient.OnMessageReceived mMessageListener) {
        this.mMessageListener = mMessageListener;
    }

    public TCPClient.OnMessageReceived getmMessageListener() {
        return mMessageListener;
    }

    public enum PTT_STATUS {
        DOWN,
        UP
    }

    private static String getDeviceID(ContentResolver contentResolver) {
        long deviceID = 0;
        final String str = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID);
        if (str != null) {
            try {
                final BigInteger bi = new BigInteger(str, 16);
                deviceID = bi.longValue();
            } catch (final NumberFormatException ex) {
                /* Nothing critical */
                Log.i(TAG, ex.toString());
            }
        }

        if (deviceID == 0) {
            /* Let's use random number */
            deviceID = new Random().nextLong();
        }

        final byte[] bb = new byte[Long.SIZE / Byte.SIZE];
        for (int idx = (bb.length - 1); idx >= 0; idx--) {
            bb[idx] = (byte) (deviceID & 0xFF);
            deviceID >>= Byte.SIZE;
        }

        return Base64.encodeToString(bb, (Base64.NO_PADDING | Base64.NO_WRAP));
    }

    public WalkieService() {
    }

    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");

        JSONDataUtils.Name name = JSONDataUtils.Name.Connect;
        String n = name.toString();
        Log.d(TAG, n);
    }

    public boolean onUnbind(Intent intent) {
        Log.d(TAG, "onUnbind");
        //m_channel.setStateListener(null);
        return false;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand: flags=" + flags + " startId=" + startId);

        if (CheckPermission()) {
            Log.d(TAG, "true");

            if (getInstance() == null) {
                instance = this; // instance is ready once linphone manager has been created
                Notification notification = createNotification();
                startForeground(1, notification);

                final String deviceID = getDeviceID(getContentResolver());

                try {
                    keyReceiver = new KeyReceiver();
                    registerReceiver(keyReceiver, keyReceiver.getIntentFilter());

                    m_audioRec = AudioRec.create(getApplicationContext());

                    audioPlayer = new AudioPlayer(this);

                    SocketConnect();

                    Thread audioplayerThread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            audioPlayer.playStream();
                        }
                    });

                    if (!audioplayerThread.isAlive()) {
                        audioplayerThread.start();
                    }
                } catch (final Exception ex) {
                    Log.w(TAG, ex.toString());
                }
            }

        }
        return Service.START_STICKY;
    }

    private Notification createNotification() {
        Intent intent = new Intent(this, WalkieActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);
        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId)
                .setContentTitle("DRC Innovation Inspire")
                .setContentText("Service is working")
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.radionetwork_icon)
                .setTicker("Ticker text")
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(channelId, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }

        return notificationBuilder.build();
    }

    public static ArrayList<Group> getGroup() {
        ArrayList<Group> groups = new ArrayList<>();
        groups.add(new Group("0", Group.GROPU_ALL, new User("101", "192.168.0.101", 5767, "101")));
        groups.add(new Group("0", Group.GROPU_ALL, new User("102", "192.168.0.102", 5768, "102")));
        groups.add(new Group("0", Group.GROPU_ALL, new User("103", "192.168.0.103", 5769, "103")));
        groups.add(new Group("0", Group.GROPU_ALL, new User("104", "192.168.0.104", 5770, "104")));
        groups.add(new Group("0", Group.GROPU_ALL, new User("105", "192.168.0.105", 5771, "105")));
        groups.add(new Group("0", Group.GROPU_ALL, new User("106", "192.168.0.106", 5772, "106")));
        groups.add(new Group("0", Group.GROPU_ALL, new User("107", "192.168.0.107", 5773, "107")));
        groups.add(new Group("0", Group.GROPU_ALL, new User("108", "192.168.0.108", 5774, "108")));
        groups.add(new Group("0", Group.GROPU_ALL, new User("109", "192.168.0.109", 5775, "109")));
        groups.add(new Group("0", Group.GROPU_ALL, new User("110", "192.168.0.110", 5776, "110")));
        groups.add(new Group("0", Group.GROPU_ALL, new User("111", "192.168.0.111", 5777, "111")));
        groups.add(new Group("0", Group.GROPU_ALL, new User("112", "192.168.0.112", 5778, "112")));
        groups.add(new Group("0", Group.GROPU_ALL, new User("113", "192.168.0.113", 5779, "113")));
        groups.add(new Group("0", Group.GROPU_ALL, new User("114", "192.168.0.114", 5780, "114")));
        groups.add(new Group("0", Group.GROPU_ALL, new User("115", "192.168.0.115", 5781, "115")));

        groups.add(new Group("1", Group.GROPU_TEAM_1, new User("101", "192.168.0.101", 5767, "101")));
        groups.add(new Group("1", Group.GROPU_TEAM_1, new User("104", "192.168.0.104", 5770, "104")));
        groups.add(new Group("1", Group.GROPU_TEAM_1, new User("105", "192.168.0.105", 5771, "105")));
        groups.add(new Group("1", Group.GROPU_TEAM_1, new User("113", "192.168.0.113", 5779, "113")));
        groups.add(new Group("1", Group.GROPU_TEAM_1, new User("114", "192.168.0.114", 5780, "114")));

        groups.add(new Group("2", Group.GROPU_TEAM_2, new User("101", "192.168.0.101", 5767, "101")));
        groups.add(new Group("2", Group.GROPU_TEAM_2, new User("102", "192.168.0.102", 5768, "102")));
        groups.add(new Group("2", Group.GROPU_TEAM_2, new User("103", "192.168.0.103", 5769, "103")));
        groups.add(new Group("2", Group.GROPU_TEAM_2, new User("104", "192.168.0.104", 5770, "104")));
        groups.add(new Group("2", Group.GROPU_TEAM_2, new User("105", "192.168.0.105", 5771, "105")));

        groups.add(new Group("3", Group.GROPU_TEAM_3, new User("106", "192.168.0.106", 5772, "106")));
        groups.add(new Group("3", Group.GROPU_TEAM_3, new User("107", "192.168.0.107", 5773, "107")));
        groups.add(new Group("3", Group.GROPU_TEAM_3, new User("108", "192.168.0.108", 5774, "108")));
        groups.add(new Group("3", Group.GROPU_TEAM_3, new User("109", "192.168.0.109", 5775, "109")));
        groups.add(new Group("3", Group.GROPU_TEAM_3, new User("110", "192.168.0.110", 5776, "110")));

        groups.add(new Group("4", Group.GROPU_TEAM_4, new User("111", "192.168.0.111", 5777, "111")));
        groups.add(new Group("4", Group.GROPU_TEAM_4, new User("112", "192.168.0.112", 5778, "112")));
        groups.add(new Group("4", Group.GROPU_TEAM_4, new User("113", "192.168.0.113", 5779, "113")));
        groups.add(new Group("4", Group.GROPU_TEAM_4, new User("114", "192.168.0.114", 5780, "114")));
        groups.add(new Group("4", Group.GROPU_TEAM_4, new User("115", "192.168.0.115", 5781, "115")));

        groups.add(new Group("5", Group.GROPU_TEAM_5, new User("116", "192.168.0.116", 5782, "116")));
        groups.add(new Group("5", Group.GROPU_TEAM_5, new User("117", "192.168.0.117", 5783, "117")));
        groups.add(new Group("5", Group.GROPU_TEAM_5, new User("118", "192.168.0.118", 5784, "118")));
        groups.add(new Group("5", Group.GROPU_TEAM_5, new User("119", "192.168.0.119", 5785, "119")));
        groups.add(new Group("5", Group.GROPU_TEAM_5, new User("120", "192.168.0.120", 5786, "120")));
        return groups;
    }

    public static ArrayList<User> getUsers() {
        ArrayList<User> users = new ArrayList<>();
        users.add(new User("101", "192.168.0.101", 5767, "101"));
        users.add(new User("102", "192.168.0.102", 5768, "102"));
        users.add(new User("103", "192.168.0.103", 5769, "103"));
        users.add(new User("104", "192.168.0.104", 5770, "104"));
        users.add(new User("105", "192.168.0.105", 5771, "105"));
        users.add(new User("106", "192.168.0.106", 5772, "106"));
        users.add(new User("107", "192.168.0.107", 5773, "107"));
        users.add(new User("108", "192.168.0.108", 5774, "108"));
        users.add(new User("109", "192.168.0.109", 5775, "109"));
        users.add(new User("110", "192.168.0.110", 5776, "110"));
        users.add(new User("111", "192.168.0.111", 5777, "111"));
        users.add(new User("112", "192.168.0.112", 5778, "112"));
        users.add(new User("113", "192.168.0.113", 5779, "113"));
        users.add(new User("114", "192.168.0.114", 5780, "114"));
        users.add(new User("115", "192.168.0.115", 5781, "115"));
        users.add(new User("116", "192.168.0.116", 5782, "116"));
        users.add(new User("117", "192.168.0.117", 5783, "117"));
        users.add(new User("118", "192.168.0.118", 5784, "118"));
        users.add(new User("119", "192.168.0.119", 5785, "119"));
        users.add(new User("120", "192.168.0.120", 5786, "120"));

        return users;

    }

    public void onDestroy() {
        Log.d(TAG, "onDestroy");

        Log.d(TAG, "onDestroy: done");

        instance = null;
        audioPlayer = null;

        try {
            unregisterReceiver(keyReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (m_audioRec != null) {
            m_audioRec.shutdown();
            m_audioRec = null;
        }

        DestroySocket();

        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    synchronized public void PlaySound(final PTT_STATUS status) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                if (status == PTT_STATUS.DOWN) {
                    try {
                        AudioManager audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
                        audioManager.setStreamVolume(AudioManager.STREAM_RING, 1, AudioManager.ADJUST_SAME);

                        Uri ringtoneUri = Uri.parse("android.resource://" + getPackageName() + "/raw/beep");
                        MediaPlayer mediaPlayer = new MediaPlayer();
                        mediaPlayer.setDataSource(getApplicationContext(), ringtoneUri);
                        mediaPlayer.setAudioStreamType(AudioManager.STREAM_RING);
                        mediaPlayer.setVolume((float) (0.15), (float) 0.15);
                        mediaPlayer.prepare();
                        mediaPlayer.start();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (status == PTT_STATUS.UP) {

                    Uri ringtoneUri = Uri.parse("android.resource://" + getPackageName() + "/raw/beep_2");
                    MediaPlayer mediaPlayer = new MediaPlayer();
                    try {
                        mediaPlayer.setDataSource(getApplicationContext(), ringtoneUri);
                        mediaPlayer.setAudioStreamType(AudioManager.STREAM_RING);
                        mediaPlayer.setVolume((float) (0.15), (float) 0.15);
                        mediaPlayer.prepare();
                        mediaPlayer.start();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        thread.start();

    }

    public boolean CheckPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
                == PackageManager.PERMISSION_GRANTED) {

            return true;
        }
        return false;
    }

    public AudioRec getAudioRecorder() {
        return m_audioRec;
    }

    public void SocketConnect() {
        conctTask = new connectTask();
        conctTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void DestroySocket() {
        try {
            mTcpClient.sendMessage("bye");
            mTcpClient.stopClient();
            conctTask.cancel(true);
            conctTask = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void SendMessage(final String message) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (mTcpClient != null) {
                        mTcpClient.sendMessage(message);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @SuppressLint("StaticFieldLeak")
    public class connectTask extends AsyncTask<String, String, TCPClient> {

        @Override
        protected TCPClient doInBackground(String... message) {

            mTcpClient = new TCPClient(getApplicationContext(), WalkieService.this);
            mTcpClient.run();

            if (mTcpClient != null) {
                mTcpClient.sendMessage("Initial Message when connected with Socket Server");
            }

            return null;
        }
    }
}
