package com.drc.radionetwork.database;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import java.util.Objects;

public class DBContentProvider extends ContentProvider {
    public DBHelper databaseHelper;
    // Create Uri for query
    public static final String authorities = "com.drc.radionetworkprovider";
    public static final Uri PROFILE_CONTENT_URI = Uri.parse("content://" + authorities + "/profile");
    public static final Uri HISTORY_CONTENT_URI = Uri.parse("content://" + authorities + "/history");

    public static final int PROFILE = 1;
    public static final int HISTORY = 2;

    private static final UriMatcher uriMatcher;

    // Allocate the UriMatcher object, where a URI ending in 'earthquakes' will
    // correspond to a request for all earthquakes, and 'earthquakes' with a
    // trailing '/[rowID]' will represent a single earthquake row.
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(authorities, "profile", PROFILE);
        uriMatcher.addURI(authorities, "history", HISTORY);

    }

    public DBContentProvider() {
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase database = databaseHelper.getWritableDatabase();

        int count;
        switch (uriMatcher.match(uri)) {
            case PROFILE:
                count = database.delete(PROFILE_TABLE.TABLE, selection,
                        selectionArgs);
                break;

            case HISTORY:
                count = database.delete(HISTORY_TABLE.TABLE, selection,
                        selectionArgs);
                break;


            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
        Objects.requireNonNull(getContext()).getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public String getType(Uri uri) {

        switch (uriMatcher.match(uri)) {
            case PROFILE:
                return "vnd.android.cursor.dir/vnd.radionetwork.profile";
            case HISTORY:
                return "vnd.android.cursor.dir/vnd.radionetwork.history";

            default:
                throw new UnsupportedOperationException("Not yet implemented");
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase database = databaseHelper.getWritableDatabase();

        switch (uriMatcher.match(uri)) {
            case PROFILE:
                // Insert the new row. The call to database.insert will return the
                // row number
                // if it is successful.
                long profile_ID = database.insert(PROFILE_TABLE.TABLE,
                        "answer", values);

                // Return a URI to the newly inserted row on success.
                if (profile_ID > 0) {
                    Uri _uri = ContentUris.withAppendedId(PROFILE_CONTENT_URI,
                            profile_ID);
                    Objects.requireNonNull(getContext()).getContentResolver().notifyChange(_uri, null);
                    return uri;
                }
                break;

            case HISTORY:
                // Insert the new row. The call to database.insert will return the
                // row number
                // if it is successful.
                long history_ID = database.insert(HISTORY_TABLE.TABLE,
                        "correctanswer", values);

                // Return a URI to the newly inserted row on success.
                if (history_ID > 0) {
                    Uri _uri = ContentUris.withAppendedId(HISTORY_CONTENT_URI,
                            history_ID);
                    Objects.requireNonNull(getContext()).getContentResolver().notifyChange(_uri, null);
                    return uri;
                }
                break;

        }
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public boolean onCreate() {
        Context context = getContext();

        databaseHelper = new DBHelper(context,
                DBHelper.DATABASE_NAME, null,
                DBHelper.DATABASE_VERSION);
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        SQLiteDatabase database = databaseHelper.getWritableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        // If this is a row query, limit the result set to the passed in row.
        switch (uriMatcher.match(uri)) {
            case PROFILE:
                qb.setTables(PROFILE_TABLE.TABLE);
                break;

            case HISTORY:
                qb.setTables(HISTORY_TABLE.TABLE);
                break;

            default:
                break;
        }

        // Apply the query to the underlying database.
        Cursor c = qb.query(database, projection, selection, selectionArgs,
                null, null, sortOrder);

        // Cursor c =
        // database.rawQuery("select * from "+SkyFrogDatabaseHelper.JOBH_TABLE,
        // null);

        // Register the contexts ContentResolver to be notified if
        // the cursor result set changes.
        c.setNotificationUri(getContext().getContentResolver(), uri);

        // Return a cursor to the query result.
        return c;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        SQLiteDatabase database = databaseHelper.getWritableDatabase();

        int count;
        switch (uriMatcher.match(uri)) {
            case PROFILE:
                count = database.update(PROFILE_TABLE.TABLE, values,
                        selection, selectionArgs);
                break;
            case HISTORY:
                count = database.update(HISTORY_TABLE.TABLE, values,
                        selection, selectionArgs);
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        //Update uri
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }
}
