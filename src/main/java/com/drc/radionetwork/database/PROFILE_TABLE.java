package com.drc.radionetwork.database;

public class PROFILE_TABLE {
    public static final String TABLE = "PROFILE_TABLE";
    public static final String ZONE = "ZONE";
    public static final String CHANEL = "CHANEL";
    public static final String RADIO_ID = "RADIO_ID";
    public static final String NAME = "NAME";

    public static String onCreate() {
        String DATABASE_CREATE = "create table "
                + TABLE
                + " ("
                + "_id" + " TEXT, "
                + ZONE + " TEXT, "
                + CHANEL + " TEXT, "
                + RADIO_ID + " TEXT, "
                + NAME + " TEXT);";
        return DATABASE_CREATE;
    }
}
