package com.drc.radionetwork.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "projecthomework.db";
    public static final int DATABASE_VERSION = 1;
    private static final String TAG = DBHelper.class.getSimpleName();

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(PROFILE_TABLE.onCreate());
        db.execSQL(HISTORY_TABLE.onCreate());

        Log.d("homework_db", "onCreate");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                + newVersion + ", which will destroy all old data");
        for (int i = oldVersion + 1; i <= newVersion; i++) {
            try {
                switch (i) {
                    case 2:
                        try {
                            /*Log.d(TAG, "Adding JOB Table");

                            // Update JobD Table
                            String sqljobd1 = "ALTER TABLE " + MY_TABLE + " ADD COLUMN " +
                                    "U_STATUSRECEIVE" + " TEXT";
                            db.execSQL(sqljobd1);

                            // Update other Table
                            db.execSQL(NEWDATABASE_CREATE);*/
                        } catch (Exception e) {

                        }
                        break;

                }
            } catch (Exception e) {
                e.printStackTrace();
                droptable(db);
            }
        }
    }

    private void droptable(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + PROFILE_TABLE.TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + HISTORY_TABLE.TABLE);

        onCreate(db);

    }
}
