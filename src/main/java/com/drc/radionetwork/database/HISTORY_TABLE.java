package com.drc.radionetwork.database;

public class HISTORY_TABLE {
    public static final String TABLE = "HISTORY_TABLE";
    public static final String ZONE = "ZONE";
    public static final String CHANEL = "CHANEL";
    public static final String RADIO_ID = "RADIO_ID";
    public static final String DATETIME = "DATETIME";

    public static String onCreate() {
        String DATABASE_CREATE = "create table "
                + TABLE
                + " ("
                + "_id" + " TEXT, "
                + ZONE + " TEXT, "
                + CHANEL + " TEXT, "
                + RADIO_ID + " TEXT, "
                + DATETIME + " TEXT);";
        return DATABASE_CREATE;
    }
}
