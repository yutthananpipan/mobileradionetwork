package com.drc.radionetwork;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.drc.radionetwork.model.Group;
import com.drc.radionetwork.model.Items;

import java.util.List;

import static com.vilyever.contextholder.ContextHolder.getContext;

public class CustomItemsAdapter extends BaseAdapter {

    private final List<Items> data;

    public CustomItemsAdapter(Context context, List<Items> data) {
        this.data = data;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Items item = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_group, parent, false);
            convertView.setTag(ViewHolder.createViewHolder(convertView));
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.textCountry.setText(item.Name);
        return convertView;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Items getItem(int position) {
        return this.data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private static class ViewHolder {
        public ImageView imgFlag;
        public TextView textCountry;

        public static ViewHolder createViewHolder(View view) {
            ViewHolder holder = new ViewHolder();
            holder.imgFlag = (ImageView) view.findViewById(R.id.imgFlag);
            holder.textCountry = (TextView) view.findViewById(R.id.textName);
            return holder;
        }
    }
}
