package com.drc.radionetwork;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.drc.radionetwork.model.History;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class HistoryActivity extends AppCompatActivity {
    ArrayList<History> dataSource = new ArrayList<History>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        ListView listView = findViewById(R.id.history_listview);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowHomeEnabled(true);

        dataSource = History.getHistories(getApplicationContext());
        HistoryAdapter adapter = new HistoryAdapter(this, dataSource);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public static class HistoryAdapter extends ArrayAdapter<History> {

        private final List<History> data;

        private static class ViewHolder {
            public TextView Zone = null;
            public TextView Channel = null;
            public TextView UID = null;
            public TextView DateTime = null;

            public static ViewHolder createViewHolder(View view) {
                ViewHolder holder = new ViewHolder();
                holder.Zone = view.findViewById(R.id.txtZone);
                holder.Channel = view.findViewById(R.id.txtChannel);
                holder.UID = view.findViewById(R.id.txtUID);
                holder.DateTime = view.findViewById(R.id.txtTimeEntrance);
                return holder;
            }
        }

        public HistoryAdapter(@NonNull Context context, @NonNull List<History> data) {
            super(context, 0, data);
            this.data = data;
        }

        @SuppressLint("SetTextI18n")
        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            History item = data.get(position);
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.history_listview_row, parent, false);
                convertView.setTag(ViewHolder.createViewHolder(convertView));
            }
            ViewHolder holder = (ViewHolder) convertView.getTag();

            try{
                holder.Zone.setText("Zone: " + item.getZone());
                holder.Channel.setText("Channel: " + item.getChannel());
                holder.UID.setText("UID: " + item.getID());
                holder.DateTime.setText("Date: " + item.getDatetime());
            }catch (Exception e){
                e.printStackTrace();
            }

            return convertView;
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            return super.getDropDownView(position, convertView, parent);
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Nullable
        @Override
        public History getItem(int position) {
            return super.getItem(position);
        }
    }
}